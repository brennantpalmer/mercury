// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Mercury.h"
#include "UnrealNetwork.h"
#include "BTPU.h"
#include "MercuryCharacter.h"
#include "PlayerUsable.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include "Runtime/Engine/Classes/Animation/AnimMontage.h"

//////////////////////////////////////////////////////////////////////////
// AMercuryCharacter


//////////////////////////////////////////////////////////////////////////
// Constructor, Replicated Variable Additions

AMercuryCharacter::AMercuryCharacter()
{

	CapsuleCrouchedHalfHeight = 60.f;
	CapsuleHalfHeight = 90.f;
	CapsuleRadius = 46.f;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(CapsuleRadius, CapsuleHalfHeight);
	
	//For debugging purposes
	GetCapsuleComponent()->SetVisibility(true);
	GetCapsuleComponent()->SetHiddenInGame(true);
	

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	//rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Configure character movement
	CrouchSpeed = 250;
	SprintSpeed = 1000;
	DefaultJogSpeed = 500;
	GetCharacterMovement()->MaxAcceleration = 500;
	GetCharacterMovement()->MaxWalkSpeed = DefaultJogSpeed;
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 275.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;
	GetCharacterMovement()->CrouchedHalfHeight = CapsuleCrouchedHalfHeight;
	GetCharacterMovement()->bCrouchMaintainsBaseLocation = true;
	GetCharacterMovement()->bCanWalkOffLedgesWhenCrouching = true;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->AttachTo(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	bCameraIsFirstPerson = false;
	bJumpButtonDown = false;
	bCrouchButtonDown = false;
	bSprintButtonDown = false;
	bADSButtonDown = false;
	bIsCrouching = false;
	bIsSprinting = false;
	bIsInADS = false;
	currentControlRotation = FRotator();
	freeLookDeltaRotation = 0.f;
	bCameraIsInFreeMode = false;
	
	
	bReplicates = true;
	bReplicateMovement = true;
	bNetLoadOnClient = false;

	ThirdPersonCameraLocation = FVector(300.f, 0.f, 70.f);
	FirstPersonCameraLocation = FVector(0.f, 0.f, 0.f);
	RightShoulderADSCameraLocation = FVector(100.f, 50.f, 70.f);
	LeftShoulderADSCameraLocation = FVector(100.f, -50.f, 70.f);
	DesiredCameraLocation = ThirdPersonCameraLocation;

	bCameraIsOnRightSide = true;
	bIsEquiping = false;
	InventoryPosition = 0;

	MyDebugHelper.ClassName = "AMercuryCharacter";
}

void AMercuryCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	MyDebugHelper.Owner = this;
	GetCharacterMovement()->MaxWalkSpeed = DefaultJogSpeed;
	currentControlRotation = GetControlRotation();
	CameraBoom->RelativeLocation.Z = 70.f;

	if (HasAuthority())
	{
		SpawnDefaultInventory();
	}
}

void AMercuryCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMercuryCharacter, bIsSprinting);
	DOREPLIFETIME(AMercuryCharacter, bIsInADS);
	DOREPLIFETIME(AMercuryCharacter, bIsCrouching);
	DOREPLIFETIME(AMercuryCharacter, currentControlRotation);
	DOREPLIFETIME(AMercuryCharacter, bJumpButtonDown);
	DOREPLIFETIME(AMercuryCharacter, bCameraIsInFreeMode); 
	DOREPLIFETIME(AMercuryCharacter, MoveRightDirectionForFreeLook); 
	DOREPLIFETIME(AMercuryCharacter, MoveForwardDirectionForFreeLook); 
	DOREPLIFETIME(AMercuryCharacter, Inventory);
	DOREPLIFETIME(AMercuryCharacter, CurrentItem);
	DOREPLIFETIME(AMercuryCharacter, bIsEquiping); 
	DOREPLIFETIME(AMercuryCharacter, InventoryPosition);
}

// This function initializes certain variables when the actor is spawned in the level
// REDUNDANT in a bad way
void AMercuryCharacter::BegingPlayInitializeVariables()
{
	
}

// This is called when this actor is spawned in the level
void AMercuryCharacter::BeginPlay()
{
	MyDebugHelper.FunctionName = "BeginPlay()";
	
	ACharacter::BeginPlay();
	BegingPlayInitializeVariables();
}


// This is called every frame
void AMercuryCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bShouldUpdateCamera)
	{
		UpdateCamera();
	}
}


//////////////////////////////////////////////////////////////////////////
// Input

void AMercuryCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// Set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAction("Jump", IE_Pressed, this, &AMercuryCharacter::InputAction_Jump_Pressed);
	InputComponent->BindAction("Jump", IE_Released, this, &AMercuryCharacter::InputAction_Jump_Released);

	InputComponent->BindAxis("MoveForward", this, &AMercuryCharacter::InputAction_MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AMercuryCharacter::InputAction_MoveRight);

	InputComponent->BindAction("Fire", IE_Pressed, this, &AMercuryCharacter::InputAction_Fire_Pressed);
	InputComponent->BindAction("Fire", IE_Released, this, &AMercuryCharacter::InputAction_Fire_Released);

	InputComponent->BindAction("Crouch", IE_Pressed, this, &AMercuryCharacter::InputAction_Crouch_Pressed);
	InputComponent->BindAction("Crouch", IE_Released, this, &AMercuryCharacter::InputAction_Crouch_Released);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &AMercuryCharacter::InputAction_Sprint_Pressed);
	InputComponent->BindAction("Sprint", IE_Released, this, &AMercuryCharacter::InputAction_Sprint_Released);

	InputComponent->BindAction("ADS", IE_Pressed, this, &AMercuryCharacter::InputAction_ADS_Pressed);
	InputComponent->BindAction("ADS", IE_Released, this, &AMercuryCharacter::InputAction_ADS_Released);

	InputComponent->BindAction("CameraToggle", IE_Pressed, this, &AMercuryCharacter::InputAction_Toggle_Camera_Pressed);
	InputComponent->BindAction("CameraToggle", IE_Released, this, &AMercuryCharacter::InputAction_Toggle_Camera_Released);

	InputComponent->BindAction("FreeCameraToggle", IE_Pressed, this, &AMercuryCharacter::InputAction_Toggle_Camera_Free_Pressed);
	InputComponent->BindAction("FreeCameraToggle", IE_Released, this, &AMercuryCharacter::InputAction_Toggle_Camera_Free_Released);

	InputComponent->BindAction("SetCameraRightShoulder", IE_Pressed, this, &AMercuryCharacter::Input_Action_SetCameraRightShoulder);

	InputComponent->BindAction("SetCameraLeftShoulder", IE_Pressed, this, &AMercuryCharacter::Input_Action_SetCameraLeftShoulder);

	InputComponent->BindAction("SwitchItemUp", IE_Pressed, this, &AMercuryCharacter::Input_Action_SwitchWeaponUp);
	InputComponent->BindAction("SwitchItemDown", IE_Pressed, this, &AMercuryCharacter::Input_Action_SwitchWeaponDown);

	InputComponent->BindAction("ChangeItemUseMode", IE_Pressed, this, &AMercuryCharacter::InputAction_ChangeCurrentItemUseMode);



	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &AMercuryCharacter::InputAction_AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &AMercuryCharacter::InputAction_TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &AMercuryCharacter::InputAction_AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &AMercuryCharacter::InputAction_LookUpAtRate);

	// handle touch devices
	InputComponent->BindTouch(IE_Pressed, this, &AMercuryCharacter::InputAction_TouchStarted);
	InputComponent->BindTouch(IE_Released, this, &AMercuryCharacter::InputAction_TouchStopped);
}


// These function return the state of the buttons that we are listening for input on
#pragma region Input Button State Getters


bool AMercuryCharacter::GetbJumpButtonDown()
{
	return bJumpButtonDown;
}
bool AMercuryCharacter::GetbCrouchButtonDown()
{
	return bCrouchButtonDown;
}
bool AMercuryCharacter::GetbSprintButtonDown()
{
	return bSprintButtonDown;
}
bool AMercuryCharacter::GetbADSButtonDown()
{
	return bADSButtonDown;
}


#pragma endregion

// Returns control rotation
FRotator AMercuryCharacter::GetCurrentControlRotation()
{
	return currentControlRotation;
}

// These functions are used to set the state of the player.
// When called, they set any incomatible states to false
#pragma  region State Setters


void AMercuryCharacter::SetStateCrouching(bool val)
{
	if (val)
	{
		if (IsSprinting())
		{
			SetStateSprinting(false);
		}

		bIsCrouching = true;
		 bShouldUpdateCamera = true;

		GetCharacterMovement()->MaxWalkSpeed = CrouchSpeed;
		
	}
	else
	{
		if (IsCrouching())
		{
			if (IsInADS())
			{
				GetCharacterMovement()->MaxWalkSpeed = CrouchSpeed;
			}
			else 
			{
				GetCharacterMovement()->MaxWalkSpeed = DefaultJogSpeed;
			}
		}
		bIsCrouching = false;
	}

	
}
void AMercuryCharacter::SetStateSprinting(bool val)
{
	if (val)
	{
		if (IsInADS())
		{
			SetStateInADS(false);
		}

		if (IsCrouching())
		{
			SetStateCrouching(false);
		}

		bIsSprinting = true;
		bShouldUpdateCamera = true;

		GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	}
	else 
	{
		if (IsSprinting())
		{
			GetCharacterMovement()->MaxWalkSpeed = DefaultJogSpeed;
		}

		bIsSprinting = false;
	}


}
void AMercuryCharacter::SetStateInADS(bool val)
{
	if (val)
	{
		if (IsSprinting())
		{
			SetStateSprinting(false);

		}

		GetCharacterMovement()->MaxWalkSpeed = CrouchSpeed;
		
		if (!bCameraIsFirstPerson)
		{
			if (bCameraIsOnRightSide)
			{
				Client_SetCameraRightShoulder();
			}
			else
			{
				Client_SetCameraLeftShoulder();
			}
		}
		
		bIsInADS = true;

		 bShouldUpdateCamera = true;

	}
	else
	{
		if (IsInADS())
		{
			GetCharacterMovement()->MaxWalkSpeed = DefaultJogSpeed;
		}

		bIsInADS = false;
	}
}

#pragma endregion

// This function is currently being called by tick and should instead be called by a timer.
// It simply determines where the camera should be based on the players state and moves the camera to that location.
void AMercuryCharacter::UpdateCamera()
{
	bool enableThisFunction = true;

	if (enableThisFunction)
	{

			bool xIsSet = false;
			bool yIsSet = false;
			bool zIsSet = true;

			///UE_LOG(LogTemp, Warning, TEXT("BEGIN AMercuryCharacter::UpdateCamera()"));


			if (bCameraIsFirstPerson)
			{
				DesiredCameraLocation = FirstPersonCameraLocation;
			}
			else if (IsSprinting())
			{
				DesiredCameraLocation = ThirdPersonCameraLocation;
			}
			else if (IsInADS())
			{
				if (bCameraIsOnRightSide)
				{
					DesiredCameraLocation = RightShoulderADSCameraLocation;
				}
				else
				{
					DesiredCameraLocation = LeftShoulderADSCameraLocation;
				}
			}
			else
			{
				DesiredCameraLocation = ThirdPersonCameraLocation;
			}




			if (CameraBoom->TargetArmLength < DesiredCameraLocation.X)
			{
				CameraBoom->TargetArmLength += 20.f;
			}
			else if (CameraBoom->TargetArmLength > DesiredCameraLocation.X)
			{
				CameraBoom->TargetArmLength -= 20.f;
			}
			else
			{
				xIsSet = true;
			}

			if (CameraBoom->RelativeLocation.Y > DesiredCameraLocation.Y + 1.f)
			{
				
				FVector Addition = FVector(0.0f, -5.f, 0.0);
				CameraBoom->AddRelativeLocation(Addition);
			}
			else if (CameraBoom->RelativeLocation.Y < DesiredCameraLocation.Y - 1.f)
			{
				FVector Addition = FVector(0.0f, 5.f, 0.0);
				CameraBoom->AddRelativeLocation(Addition);
			}
			else 
			{
				yIsSet = true;
			}

			/*if (CameraBoom->RelativeLocation.Z < DesiredCameraLocation.Z)
			{
				CameraBoom->RelativeLocation.Z += 5.f;
			}
			else if (CameraBoom->RelativeLocation.Z > DesiredCameraLocation.Z)
			{
				CameraBoom->RelativeLocation.Z -= 5.f;
			}*/

			if (zIsSet && yIsSet && xIsSet)
			{
				bShouldUpdateCamera = false;
			}
			//UE_LOG(LogTemp, Warning, TEXT("END   AMercuryCharacter::UpdateCamera()"));

		
	}
}

// These functions return whether the player is in a particualr state
#pragma region State Getters


bool AMercuryCharacter::IsCrouching()
{
	return bIsCrouching;
}
bool AMercuryCharacter::IsSprinting()
{
	return bIsSprinting;
}
bool AMercuryCharacter::IsInADS()
{
	return bIsInADS;
}


#pragma endregion

// These functions are called to determine whether a player can enter into states
#pragma region Validation to enter State

bool AMercuryCharacter::CanCrouch()
{
	if (GetCharacterMovement()->IsFalling() || !ACharacter::CanCrouch() || !GetCharacterMovement()->CanEverCrouch())
	{
		return false;
	}
	return true;

}
bool AMercuryCharacter::CanJump()
{
	if (GetCharacterMovement()->IsFalling() || IsCrouching() || IsInADS())
	{
		return false;
	}
	return true;

}
bool AMercuryCharacter::CanSprint()
{
	if (GetCharacterMovement()->IsFalling())
	{
		return false;
	}
	return true;
}
bool AMercuryCharacter::CanGoIntoADS()
{
	if (GetCharacterMovement()->IsFalling())
	{
		return false;
	}
	return true;

}


#pragma endregion

// Functions that are called by the Player input component. 
// These functions call the server and client input handlers
#pragma region Input Actions


void AMercuryCharacter::InputAction_Toggle_Camera_Pressed()
{
	Client_Toggle_Camera_Pressed();
}

void AMercuryCharacter::InputAction_Toggle_Camera_Released()
{
	Client_Toggle_Camera_Released();
}

void AMercuryCharacter::InputAction_Toggle_Camera_Free_Pressed()
{
	Client_Toggle_Camera_Free_Pressed();
	Server_Toggle_Camera_Free_Pressed();
}
void AMercuryCharacter::InputAction_Toggle_Camera_Free_Released()
{
	Client_Toggle_Camera_Free_Released();
	Server_Toggle_Camera_Free_Released();
}

void AMercuryCharacter::InputAction_Jump_Pressed()
{
	Client_Jump_Pressed();
	Server_Jump_Pressed();
}

void AMercuryCharacter::InputAction_Jump_Released()
{
	Client_Jump_Released();
	Server_Jump_Released();
}

void AMercuryCharacter::InputAction_Fire_Pressed()
{
	Client_Fire_Pressed();
	//Server_Fire_Pressed();
}

void AMercuryCharacter::InputAction_Fire_Released()
{
	Client_Fire_Released();
	//Server_Fire_Released();
}

void AMercuryCharacter::InputAction_Crouch_Pressed()
{
	Client_Crouch_Pressed();
	Server_Crouch_Pressed();
}

void AMercuryCharacter::InputAction_Crouch_Released()
{
	Client_Crouch_Released();
	Server_Crouch_Released();
}

void AMercuryCharacter::InputAction_ADS_Pressed()
{
	Client_ADS_Pressed();
	Server_ADS_Pressed();
}

void AMercuryCharacter::InputAction_ADS_Released()
{
	Client_ADS_Released();
	Server_ADS_Released();
}

void AMercuryCharacter::InputAction_Sprint_Pressed()
{
	Client_Sprint_Pressed();
	Server_Sprint_Pressed();
}

void AMercuryCharacter::InputAction_Sprint_Released()
{
	Client_Sprint_Released();
	Server_Sprint_Released();
}

void AMercuryCharacter::InputAction_AddControllerYawInput(float val)
{
	Server_AddControllerYawInput(val);
	Client_AddControllerYawInput(val);
}

void AMercuryCharacter::InputAction_AddControllerPitchInput(float val)
{
	Client_AddControllerPitchInput(val);
	Server_AddControllerPitchInput(val);
}

void AMercuryCharacter::Input_Action_SetCameraLeftShoulder()
{
	Client_SetCameraLeftShoulder();
}
void AMercuryCharacter::Input_Action_SetCameraRightShoulder()
{
	Client_SetCameraRightShoulder();
}



void AMercuryCharacter::InputAction_TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void AMercuryCharacter::InputAction_TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void AMercuryCharacter::InputAction_TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMercuryCharacter::InputAction_LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AMercuryCharacter::InputAction_MoveForward(float Value)
{
	Client_MoveForward(Value);
	Server_MoveForward(Value);
}

void AMercuryCharacter::InputAction_MoveRight(float Value)
{
	Client_MoveRight(Value);
	Server_MoveRight(Value);
}

void AMercuryCharacter::Input_Action_SwitchWeaponUp()
{
	//Client_SwitchWeaponUp();
	Server_SwitchWeaponUp();
}

void AMercuryCharacter::Input_Action_SwitchWeaponDown()
{
	//Client_SwitchWeaponDown();
	Server_SwitchWeaponDown();
}

void AMercuryCharacter::InputAction_ChangeCurrentItemUseMode()
{
	Client_ChangeCurrentItemUseMode();
	Server_ChangeCurrentItemUseMode();
}

#pragma endregion

// Client Input Handlers
// These functions are called by the input action functions
// They run on the client side only
#pragma region Client Input Handlers


void AMercuryCharacter::Client_Jump_Pressed()
{
	bJumpButtonDown = true;

	if (CanJump())
	{
		ACharacter::Jump();
		SetStateCrouching(false);
		SetStateInADS(false);
		SetStateSprinting(false);
	}
}

void AMercuryCharacter::Client_Jump_Released()
{
	bJumpButtonDown = false;
	ACharacter::StopJumping();
}

void AMercuryCharacter::Client_AddControllerYawInput(float val)
{
	
		/*UE_LOG(LogTemp, Warning, TEXT("BEGIN AMercuryCharacter::Client_AddControllerYawInput(float val)"));
		UE_LOG(LogTemp, Warning, TEXT("////////////////////////////////////////////////////////////////"));
		UE_LOG(LogTemp, Warning, TEXT("AMercuryCharacter::Client_AddControllerYawInput(float val): Input Value: %f"), val);*/
	
	
	if (!HasAuthority() || HasAuthority())
	{

		if (val > 3.f)
		{
		val = 3.f;
		}else if (val < -3.f)
		{
		val = -3.f;
		}


		if (bCameraIsInFreeMode)
		{

			//UE_LOG(LogTemp, Warning, TEXT("AMercuryCharacter::Client_AddControllerYawInput(float val): freeLookDeltaRotation: %f"), freeLookDeltaRotation);



			if (freeLookDeltaRotation < 30.f && val > 0.f)
			{
				//UE_LOG(LogTemp, Warning, TEXT("Excecuting AddController Input because: freeLookDeltaRotation < 90.f && val > 0.f"));
				APawn::AddControllerYawInput(val);
				freeLookDeltaRotation += val;
			}
			else if (freeLookDeltaRotation > -30.f && val < 0.f)
			{

				//UE_LOG(LogTemp, Warning, TEXT("Excecuting AddController Input because: freeLookDeltaRotation > -90.f && val < 0.f"));
				APawn::AddControllerYawInput(val);
				freeLookDeltaRotation += val;
			}
			//UE_LOG(LogTemp, Warning, TEXT("Did not introduce input to controller"));
		}
		else
		{
			APawn::AddControllerYawInput(val);
		}

	}
	currentControlRotation = GetControlRotation();
	/*UE_LOG(LogTemp, Warning, TEXT("////////////////////////////////////////////////////////////////"));
	UE_LOG(LogTemp, Warning, TEXT("END  AMercuryCharacter::Client_AddControllerYawInput(float val)"));*/

}

void AMercuryCharacter::Client_AddControllerPitchInput(float val)
{
	
	if (!HasAuthority() || HasAuthority())
	{
		if (val > 3.f)
		{
			val = 3.f;
		}
		else if (val < -3.f)
		{
			val = -3.f;
		}

		APawn::AddControllerPitchInput(val);
	}
	currentControlRotation = GetControlRotation();


	// This is just a temporary fix for the mesh not being lined up properly
	currentControlRotation.Pitch += 5.f;
}

void AMercuryCharacter::Client_MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{

		if (bCameraIsInFreeMode)
		{
			AddMovementInput(MoveForwardDirectionForFreeLook, Value);
		}
		else 
		{

			// find out which way is forward
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			// get forward vector
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
			AddMovementInput(Direction, Value);
		}
		
	}
}

void AMercuryCharacter::Client_MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{

		if (bCameraIsInFreeMode)
		{
			AddMovementInput(MoveRightDirectionForFreeLook, Value);
		}
		else 
		{
			// find out which way is right
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			// get right vector 
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
			// add movement in that direction
			AddMovementInput(Direction, Value);
		}
		
	}
}

void AMercuryCharacter::Client_Fire_Pressed()
{
	if (CurrentItem)
	{
		CurrentItem->Input_StartUse();
	}
}

void AMercuryCharacter::Client_Fire_Released()
{
	if (CurrentItem)
	{
		CurrentItem->Input_StopUse();
	}
}

void AMercuryCharacter::Client_Crouch_Pressed()
{
	
	bCrouchButtonDown = true;

	if (CanCrouch())
	{
		GetCharacterMovement()->bWantsToCrouch = true;
		SetStateCrouching(true);
	}
}

void AMercuryCharacter::Client_Crouch_Released()
{
	bCrouchButtonDown = false;
	GetCharacterMovement()->bWantsToCrouch = false;
	SetStateCrouching(false);
}

void AMercuryCharacter::Client_ADS_Pressed()
{
	bADSButtonDown = true;


	if (CanGoIntoADS())
	{
		SetStateInADS(true);
	}
}

void AMercuryCharacter::Client_ADS_Released()
{
	bADSButtonDown = false;
	SetStateInADS(false);
	bShouldUpdateCamera = true;
	
}

void AMercuryCharacter::Client_Sprint_Pressed()
{
	bSprintButtonDown = true;
	if (CanSprint())
	{
		SetStateSprinting(true);
	}
}

void AMercuryCharacter::Client_Sprint_Released()
{
	bSprintButtonDown = false;
	SetStateSprinting(false);
}

void AMercuryCharacter::Client_Toggle_Camera_Pressed()
{
	if (bCameraIsFirstPerson)
	{
		bCameraIsFirstPerson = false;
		GetMesh()->SetVisibility(true);
		bShouldUpdateCamera = true;

	}
	else
	{
		bCameraIsFirstPerson = true;
		CameraBoom->TargetArmLength = 0.f;
		GetMesh()->SetVisibility(false);
		bShouldUpdateCamera = true;
	}
}

void AMercuryCharacter::Client_Toggle_Camera_Released()
{

}

void AMercuryCharacter::Client_Toggle_Camera_Free_Pressed()
{
	bCameraIsInFreeMode = true;
	bUseControllerRotationYaw = false;
	freeLookDeltaRotation = 0.f;

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	// get right vector 
	MoveRightDirectionForFreeLook = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	MoveForwardDirectionForFreeLook = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
}
void AMercuryCharacter::Client_Toggle_Camera_Free_Released()
{
	bCameraIsInFreeMode = false;
	bUseControllerRotationYaw = true;
	freeLookDeltaRotation = 0.f;

}

void AMercuryCharacter::Client_SetCameraLeftShoulder()
{
	bCameraIsOnRightSide = false;
	 bShouldUpdateCamera = true;
}
void AMercuryCharacter::Client_SetCameraRightShoulder()
{
	bCameraIsOnRightSide = true;
	 bShouldUpdateCamera = true;
}

void AMercuryCharacter::Client_SwitchWeaponUp()
{
	if (InventoryPosition < Inventory.Num() - 1)
	{
		InventoryPosition++;
		EquipItem(Inventory[InventoryPosition]);
	}
}

void AMercuryCharacter::Client_SwitchWeaponDown()
{
	if (InventoryPosition > 0)
	{
		InventoryPosition--;
		EquipItem(Inventory[InventoryPosition]);
	}
}

void AMercuryCharacter::Client_ChangeCurrentItemUseMode()
{
	if (CurrentItem)
	{
		CurrentItem->Input_SwitchUseMode();
	}
}

#pragma endregion

// Server Input Handlers
// These functions are called by the input action functions
// They carry out server side input
#pragma region Server Input Handlers


void AMercuryCharacter::Server_Jump_Pressed_Implementation()
{
	Client_Jump_Pressed();
}
bool AMercuryCharacter::Server_Jump_Pressed_Validate()
{
	return true;
}




void AMercuryCharacter::Server_Jump_Released_Implementation()
{
	Client_Jump_Released();
}
bool AMercuryCharacter::Server_Jump_Released_Validate()
{
	return true;
}




void AMercuryCharacter::Server_Fire_Pressed_Implementation()
{
	Client_Fire_Pressed();
}
bool AMercuryCharacter::Server_Fire_Pressed_Validate()
{
	return true;
}




void AMercuryCharacter::Server_Fire_Released_Implementation()
{
	Client_Fire_Released();
}
bool AMercuryCharacter::Server_Fire_Released_Validate()
{
	return true;
}




void AMercuryCharacter::Server_Crouch_Pressed_Implementation()
{
	Client_Crouch_Pressed();
}
bool AMercuryCharacter::Server_Crouch_Pressed_Validate()
{
	return true;
}




void AMercuryCharacter::Server_Crouch_Released_Implementation()
{
	Client_Crouch_Released();
}
bool AMercuryCharacter::Server_Crouch_Released_Validate()
{
	return true;
}




void AMercuryCharacter::Server_ADS_Pressed_Implementation()
{
	Client_ADS_Pressed();
}
bool AMercuryCharacter::Server_ADS_Pressed_Validate()
{
	return true;
}




void AMercuryCharacter::Server_ADS_Released_Implementation()
{
	Client_ADS_Released();
}
bool AMercuryCharacter::Server_ADS_Released_Validate()
{
	return true;
}




void AMercuryCharacter::Server_Sprint_Pressed_Implementation()
{
	Client_Sprint_Pressed();
}
bool AMercuryCharacter::Server_Sprint_Pressed_Validate()
{
	return true;
}




void AMercuryCharacter::Server_Sprint_Released_Implementation()
{
	Client_Sprint_Released();
}
bool AMercuryCharacter::Server_Sprint_Released_Validate()
{
	return true;
}




void AMercuryCharacter::Server_MoveForward_Implementation(float Value)
{
	//Client_MoveForward(Value);
}
bool AMercuryCharacter::Server_MoveForward_Validate(float Value)
{
	return true;
}




void AMercuryCharacter::Server_MoveRight_Implementation(float Value)
{
	//Client_MoveRight(Value);
}
bool AMercuryCharacter::Server_MoveRight_Validate(float Value)
{
	return true;
}




void AMercuryCharacter::Server_AddControllerYawInput_Implementation(float val)
{
	Client_AddControllerYawInput(val);
}
bool AMercuryCharacter::Server_AddControllerYawInput_Validate(float val)
{
	return true;
}




void AMercuryCharacter::Server_AddControllerPitchInput_Implementation(float val)
{
	Client_AddControllerPitchInput(val);
}
bool AMercuryCharacter::Server_AddControllerPitchInput_Validate(float val)
{
	return true;
}


void AMercuryCharacter::Server_Toggle_Camera_Free_Pressed_Implementation()
{
	Client_Toggle_Camera_Free_Pressed();
}
bool AMercuryCharacter::Server_Toggle_Camera_Free_Pressed_Validate()
{
	return true;

}

void AMercuryCharacter::Server_Toggle_Camera_Free_Released_Implementation()
{
	Client_Toggle_Camera_Free_Released();
}
bool AMercuryCharacter::Server_Toggle_Camera_Free_Released_Validate()
{
	return true;

}

void AMercuryCharacter::Server_SetCameraLeftShoulder_Implementation()
{
	
}

bool AMercuryCharacter::Server_SetCameraLeftShoulder_Validate()
{
	return true;
}


void AMercuryCharacter::Server_SetCameraRightShoulder_Implementation()
{

}
bool AMercuryCharacter::Server_SetCameraRightShoulder_Validate()
{
	return true;
}

void AMercuryCharacter::Server_SwitchWeaponUp_Implementation()
{
	Client_SwitchWeaponUp();
}

bool AMercuryCharacter::Server_SwitchWeaponUp_Validate()
{
	return true;
}

void AMercuryCharacter::Server_SwitchWeaponDown_Implementation()
{
	Client_SwitchWeaponDown();
}

bool AMercuryCharacter::Server_SwitchWeaponDown_Validate()
{
	return true;
}

void AMercuryCharacter::Server_ChangeCurrentItemUseMode_Implementation()
{
	Client_ChangeCurrentItemUseMode();
}

bool AMercuryCharacter::Server_ChangeCurrentItemUseMode_Validate()
{
	return true;
}

#pragma endregion



//Inventory and items

FName AMercuryCharacter::GetRightHandAttachPoint() const
{
	return RightHandAttachPoint;
}

void AMercuryCharacter::SpawnDefaultInventory()
{
	InventoryPosition = 0;

	int32 NumWeaponClasses = DefaultInventoryClasses.Num();
	for (int32 i = 0; i < NumWeaponClasses; i++)
	{
		if (DefaultInventoryClasses[i])
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.Owner = this;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			APlayerUsable* Item = GetWorld()->SpawnActor<APlayerUsable>(DefaultInventoryClasses[i], SpawnInfo);
			AddItemToInventory(Item);
		}
	}

	// equip first weapon in inventory
	if (Inventory.Num() > 0)
	{
		EquipItem(Inventory[0]);
	}
}


void AMercuryCharacter::RemoveItemFromInventory(APlayerUsable* Item)
{
	if (Item)
	{

		if (CurrentItem == Item)
		{
			CurrentItem = NULL;
		}
		Inventory.RemoveSingle(Item);
		Item->OnLeaveInventory();
	}
}

void AMercuryCharacter::EquipItem(APlayerUsable* Item)
{

	if (!HasAuthority())
	{
		return;
	}

	if (Item)
	{

		Item->SetMyOwner(this);

		float Duration = 2.0;

		
		Multicast_PlayAnimMontage(Item->GetItemEquipAnim().Montage1p);
		
			//PlayAnimMontage(Item->GetItemEquipAnim().Montage1p, 1.f, NAME_None);
		
		
		if (Duration <= 0.0f)
		{
			// failsafe
			Duration = 0.5f;
		}

		CurrentItem = Item;
		SetEquiping(true); 

		GetWorldTimerManager().SetTimer(TimerHandle_OnEquipFinished, this, &AMercuryCharacter::OnFinishEquipItem, Duration, false);
		GetWorldTimerManager().SetTimer(TimerHandle_OnEquipAttachItem, this, &AMercuryCharacter::OnEquipItemCurrentItem, Duration / 2.f, false);
	}

}

void AMercuryCharacter::OnFinishEquipItem()
{
	SetEquiping(false);
}

void AMercuryCharacter::UnEquipItem()
{
	if (CurrentItem)
	{
		CurrentItem->OnUnEquip();
	}
}


void AMercuryCharacter::UnEquipAllItems()
{
	for (auto v : Inventory)
	{
		v->OnUnEquip();
	}
}

void AMercuryCharacter::OnEquipItemCurrentItem()
{
	UnEquipAllItems();
	if (CurrentItem)
	{
		CurrentItem->OnEquip();
	}
}

//Attaches new item and detached old item. 
void AMercuryCharacter::Anim_Notify_AttachItem()
{
	if (CurrentItem)
	{
		UnEquipItem();
		CurrentItem->OnEquip();
	}
}


void AMercuryCharacter::AddItemToInventory(APlayerUsable* Item)
{
	if (Item)
	{
		Inventory.AddUnique(Item);
		Item->OnEnterInventory(this);
	}
}

void AMercuryCharacter::SetEquiping_Implementation(bool shouldBeEquiping)
{
	bIsEquiping = shouldBeEquiping;
}

bool AMercuryCharacter::SetEquiping_Validate(bool shouldBeEquiping)
{
	return true;
}

bool AMercuryCharacter::IsEquiping()
{
	return bIsEquiping;
}


/////////////////////////////////////////////////////////////////////////////
// Animations

float AMercuryCharacter::PlayAnimMontage(UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	MyDebugHelper.FunctionName = "PlayAnimMontage(UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)";
	MyDebugHelper.PrintToConsole();

	USkeletalMeshComponent* UseMesh = GetMesh();
	if (AnimMontage && UseMesh && UseMesh->AnimScriptInstance)
	{
		return UseMesh->AnimScriptInstance->Montage_Play(AnimMontage, InPlayRate);
	}

	return 0.0f;
}

void AMercuryCharacter::Multicast_PlayAnimMontage_Implementation(UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	if (!HasAuthority())
	{
		MyDebugHelper.FunctionName = "Multicast_PlayAnimMontage_Implementation(UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)";
		MyDebugHelper.PrintToConsole();
		PlayAnimMontage(AnimMontage, InPlayRate, StartSectionName);
	}
}


void AMercuryCharacter::StopAnimMontage(UAnimMontage* AnimMontage)
{

	MyDebugHelper.FunctionName = "StopAnimMontage(UAnimMontage* AnimMontage)";
	MyDebugHelper.PrintToConsole();

	USkeletalMeshComponent* UseMesh = GetMesh();
	if (AnimMontage && UseMesh && UseMesh->AnimScriptInstance &&
		UseMesh->AnimScriptInstance->Montage_IsPlaying(AnimMontage))
	{
		UseMesh->AnimScriptInstance->Montage_Stop(AnimMontage->BlendOut.GetBlendTime());
	}
}