// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "MercuryGameMode.generated.h"

UCLASS(minimalapi)
class AMercuryGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AMercuryGameMode();
};



