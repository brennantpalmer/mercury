// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "BTPU.h"
#include "MercuryCharacter.generated.h"



UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPlayerLocomtionState : uint8
{
	IdleWalkRun	UMETA(DisplayName = "HipJoging"),
	Sprinting 	UMETA(DisplayName = "Sprinting"),
	Crouching	UMETA(DisplayName = "Crouching")
};

UCLASS(config=Game)
class AMercuryCharacter : public ACharacter
{
	GENERATED_BODY()

		///////////////////////////////////////////////////////////////////////////////////////////
		///////// Camera

		DebugHelper MyDebugHelper;

#pragma region Camera

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

#pragma endregion
public:
	AMercuryCharacter();

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	///////////////////////////////////////////////////////////////////////////////////////////
	///////// Movement Defaults

#pragma region Movement Defaults
public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	float CrouchSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	float SprintSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
	float DefaultJogSpeed;

#pragma endregion

protected:
	virtual void BeginPlay() override;

	void BegingPlayInitializeVariables();

	virtual void Tick(float DeltaTime);

	/** spawn inventory, setup initial variables */
	virtual void PostInitializeComponents() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;

protected:

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface



#pragma region Camera Variables and states
private:
	//Camera States

	bool bCameraIsFirstPerson;

	UPROPERTY(Replicated)
	bool bCameraIsInFreeMode;

	// Variables for camera functions
	const FTimerDelegate CameraInterpDelegate = FTimerDelegate::CreateUObject(this, &AMercuryCharacter::UpdateCamera);

	// Handle for the timer that repeatedly calls the Update Camera function when player changes camera posititon
	FTimerHandle CameraInterpHandle;

	UPROPERTY(Replicated)
		float freeLookDeltaRotation;
	UPROPERTY(Replicated)
		FVector MoveRightDirectionForFreeLook;

	// This holds the direction that the player was looking when it enterd "free look mode." 
	// it is used because we want to restrict the movement of the player while in that mode
	UPROPERTY(Replicated)
	FVector MoveForwardDirectionForFreeLook;


	// These are the relative locaions for the different camera positions
	FVector ThirdPersonCameraLocation;
	FVector FirstPersonCameraLocation;
	FVector RightShoulderADSCameraLocation;
	FVector LeftShoulderADSCameraLocation;

	// This is the relative location that the camera wants to be in. it is always one of the locations above
	FVector DesiredCameraLocation;


	bool bShouldUpdateCamera;
	void UpdateCamera();


#pragma endregion

	// Button states

#pragma region Button States
private:

	UPROPERTY(Replicated)
	bool bJumpButtonDown;

	bool bCrouchButtonDown;
	bool bSprintButtonDown;
	bool bADSButtonDown;
	bool bCameraIsOnRightSide;

#pragma endregion

	// Mercury Character Gameplay States

#pragma region Mercury Character Gameplay States
private:

	UPROPERTY(Replicated)
	bool bIsCrouching;
	UPROPERTY(Replicated)
	bool bIsSprinting;
	UPROPERTY(Replicated)
	bool bIsInADS;
	UPROPERTY(Replicated)
	bool bIsEquiping;
	UPROPERTY(Replicated)
	FRotator currentControlRotation;

#pragma endregion

	// Variables that configure crouch

#pragma region Crouch Variables
private:

	float CapsuleCrouchedHalfHeight;
	float CapsuleHalfHeight;
	float CapsuleRadius;

#pragma endregion

	
	// State Setters

#pragma region State Setters
	public:

	//Public Blueprint exposed function for now but will change in final 
	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation, Category = GameplayStates)
	void SetEquiping(bool shouldBeEquiping);

private:

	void SetStateCrouching(bool val);
	void SetStateSprinting(bool val);
	void SetStateInADS(bool val);

#pragma endregion



	// Button state Getters
#pragma region Button State Getters

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = ButtonStates)
	bool GetbJumpButtonDown();
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = ButtonStates)
	bool GetbCrouchButtonDown();
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = ButtonStates)
	bool GetbSprintButtonDown();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = ButtonStates)
	bool GetbADSButtonDown();

#pragma endregion



	UFUNCTION(BlueprintCallable, BlueprintPure, Category = ButtonStates)
	FRotator GetCurrentControlRotation();
	
	
	/////////////////////////////////////////////////////////////////////
	////Mercury Character State Getters

#pragma region Mercury Character State Getters

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = PlayerStates)
	bool IsCrouching();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = PlayerStates)
	bool IsSprinting();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = PlayerStates)
	bool IsInADS();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = PlayerStates)
	bool IsEquiping();

#pragma endregion

	//Validation for entering states

#pragma region Enter State Validation

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = PlayerStates)
	bool CanCrouch();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = PlayerStates)
	bool CanJump();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = PlayerStates)
	bool CanSprint();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = PlayerStates)
	bool CanGoIntoADS();

#pragma endregion




	// Input Actions
	// Functions that are called by the Player input component. 
	// These functions call the server and client input handlers
#pragma region Input Actions

protected:

	void InputAction_MoveForward(float Value);
	void InputAction_MoveRight(float Value);
	void InputAction_TurnAtRate(float Rate);
	void InputAction_LookUpAtRate(float Rate);
	void InputAction_TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);
	void InputAction_TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);
	void InputAction_Toggle_Camera_Pressed();
	void InputAction_Toggle_Camera_Released();
	void InputAction_Toggle_Camera_Free_Pressed();
	void InputAction_Toggle_Camera_Free_Released();
	void InputAction_Jump_Pressed();
	void InputAction_Jump_Released();
	void InputAction_Fire_Pressed();
	void InputAction_Fire_Released();
	void InputAction_Crouch_Pressed();
	void InputAction_Crouch_Released();
	void InputAction_ADS_Pressed();
	void InputAction_ADS_Released();
	void InputAction_Sprint_Pressed();
	void InputAction_Sprint_Released();
	void InputAction_AddControllerYawInput(float val);
	void InputAction_AddControllerPitchInput(float val);
	void Input_Action_SetCameraLeftShoulder();
	void Input_Action_SetCameraRightShoulder();
	void Input_Action_SwitchWeaponUp();
	void Input_Action_SwitchWeaponDown();
	void InputAction_ChangeCurrentItemUseMode();



#pragma endregion

	// Client Input Handlers
	// These functions are called by the input action functions
	// They run on the client side only

#pragma region Client Input Handlers

	protected:
	void Client_Toggle_Camera_Pressed();
	void Client_Toggle_Camera_Released();
	void Client_Toggle_Camera_Free_Pressed();
	void Client_Toggle_Camera_Free_Released();
	void Client_Jump_Pressed();
	void Client_Jump_Released();
	void Client_Fire_Pressed();
	void Client_Fire_Released();
	void Client_Crouch_Pressed();
	void Client_Crouch_Released();
	void Client_ADS_Pressed();
	void Client_ADS_Released();
	void Client_Sprint_Pressed();
	void Client_Sprint_Released();
	void Client_MoveForward(float Value);
	void Client_MoveRight(float Value);
	void Client_AddControllerYawInput(float val);
	void Client_AddControllerPitchInput(float val);
	void Client_SetCameraLeftShoulder();
	void Client_SetCameraRightShoulder();
	void Client_SwitchWeaponUp();
	void Client_SwitchWeaponDown();
	void Client_ChangeCurrentItemUseMode();


#pragma endregion


	// Server Input Handlers
	// These functions are called by the input action functions
	// They carry out server side input


#pragma region Server Input Handlers
	protected:

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Jump_Pressed();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Jump_Released();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Fire_Pressed();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Fire_Released();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Crouch_Pressed();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Crouch_Released();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_ADS_Pressed();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_ADS_Released();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Sprint_Pressed();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Sprint_Released();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_MoveForward(float Value);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_MoveRight(float Value);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_AddControllerYawInput(float val);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_AddControllerPitchInput(float val);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Toggle_Camera_Free_Pressed();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Toggle_Camera_Free_Released();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SetCameraLeftShoulder();
	
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SetCameraRightShoulder();

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_SwitchWeaponUp();

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_SwitchWeaponDown();

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_ChangeCurrentItemUseMode();

#pragma endregion



//Getters for camera components

#pragma region Camera Components Getters

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

#pragma endregion


/////////////////////////////////////////////////////////////////////////////
	//Inventory

#pragma region Inventory
	public:

	UFUNCTION(BlueprintCallable, Category = Animation)
		void Anim_Notify_AttachItem();
	
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None);

protected:

	// Keeps track of where we are in our inventory list
	UPROPERTY(Replicated)
	int32 InventoryPosition;

	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
		FName RightHandAttachPoint;

	// Inventory and Items
	FName GetRightHandAttachPoint() const;

	/** default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
		TArray<TSubclassOf<class APlayerUsable> > DefaultInventoryClasses;

	/** weapons in inventory */
	UPROPERTY(Replicated)
		TArray<class APlayerUsable*> Inventory;

	/** currently equipped weapon */
	UPROPERTY(Replicated)
		class APlayerUsable* CurrentItem;

	
	/** [server] spawns default inventory */
	void SpawnDefaultInventory();

	void EquipItem(class APlayerUsable* Item);
	void OnFinishEquipItem();
	void UnEquipItem();

	void AddItemToInventory(APlayerUsable* Item);
	void RemoveItemFromInventory(APlayerUsable* Item);

	void UnEquipAllItems();

	void OnEquipItemCurrentItem();



#pragma endregion

	public:
		/** Handle for efficient management of OnEquipFinished timer */
		FTimerHandle TimerHandle_OnEquipFinished;

		/** Handle for efficient management of OnEquipFinished timer */
		FTimerHandle TimerHandle_OnEquipAttachItem;

		/** Handle for efficient management of OnEquipFinished timer */
		FTimerHandle TimerHandle_OnEquipDetachItem;


	//////////////////////////////////////////////////////////////////////////
	//// Animations

	/** play anim montage */
	virtual float PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) override;

	/** stop playing montage */
	virtual void StopAnimMontage(class UAnimMontage* AnimMontage) override;
};

