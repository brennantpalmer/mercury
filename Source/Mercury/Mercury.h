// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#ifndef __MERCURY_H__
#define __MERCURY_H__

#include "EngineMinimal.h"

/** when you modify this, please note that this information can be saved with instances
* also DefaultEngine.ini [/Script/Engine.CollisionProfile] should match with this list **/
#define COLLISION_WEAPON		ECC_GameTraceChannel1
#define COLLISION_PROJECTILE	ECC_GameTraceChannel2
#define COLLISION_PICKUP		ECC_GameTraceChannel3

#endif
