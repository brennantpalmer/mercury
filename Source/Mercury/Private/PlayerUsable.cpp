// Fill out your copyright notice in the Description page of Project Settings.

#include "Mercury.h"
#include "UnrealNetwork.h"
#include "PlayerUsable.h"
#include "MercuryCharacter.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include "BTPU.h"
#include "Runtime/Engine/Classes/Animation/AnimMontage.h"




// Sets default values
APlayerUsable::APlayerUsable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh1P"));
	Mesh1P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->CastShadow = false;
	//Mesh1P->bEnablePhysicsOnDedicatedServer = false;
	//Mesh1P->SetSimulatePhysics(false);
	Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	RootComponent = Mesh1P;

	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh3P"));
	Mesh3P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh3P->bReceivesDecals = false;
	//Mesh3P->SetSimulatePhysics(false);
	//Mesh3P->bEnablePhysicsOnDedicatedServer = false;
	Mesh3P->CastShadow = true;
	Mesh3P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
	
	
	Mesh3P->SetupAttachment(Mesh1P);
	Mesh3P->SetHiddenInGame(false);

	bReplicateMovement = true;
	bReplicates = true;
	bNetLoadOnClient = true;
	
	PlayerUsableDebugHelper.ClassName = "APlayerUsable";
	PlayerUsableDebugHelper.bShouldPrint = false;

	bIsAttached = false;
	bIsEuipped = false;
	bIsInSingleMode = true;
	bIsInBurstMode = false;
	bIsInAutomaticMode = false;
	bIsOwned = false;
}

void APlayerUsable::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APlayerUsable, MyOwner);
	DOREPLIFETIME(APlayerUsable, bIsAttached);
	DOREPLIFETIME(APlayerUsable, bIsEuipped);
	DOREPLIFETIME(APlayerUsable, bIsInSingleMode);
	DOREPLIFETIME(APlayerUsable, bIsInBurstMode);
	DOREPLIFETIME(APlayerUsable, bIsInAutomaticMode);
	DOREPLIFETIME(APlayerUsable, bIsOwned);

}

// Called when the game starts or when spawned
void APlayerUsable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerUsable::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

const AMercuryCharacter* APlayerUsable::GetMyOwner()
{
	const AMercuryCharacter* returnPointer = MyOwner;
	return returnPointer;
}

void APlayerUsable::SetMyOwner(AMercuryCharacter* newOwner)
{
	if (newOwner && (newOwner != MyOwner))
	{
		SetOwner(newOwner);
		Instigator = newOwner;
		MyOwner = newOwner;
	}
}


FItemConfig APlayerUsable::GetItemConfig()
{
	return ItemConfig;
}


void APlayerUsable::AttachMeshToPawn()
{
	if (MyOwner)
	{

		// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
		
			static FName AttachPoint(TEXT("RightHand"));
			USkeletalMeshComponent* PawnMesh3p = MyOwner->GetMesh();
			Mesh1P->SetHiddenInGame(true);
			Mesh3P->SetHiddenInGame(false);
			Mesh1P->AttachTo(PawnMesh3p, AttachPoint);
			Mesh3P->AttachTo(PawnMesh3p, AttachPoint);

			AttachRootComponentTo(MyOwner->GetMesh(), AttachPoint, EAttachLocation::SnapToTarget, true);
	}
}

void APlayerUsable::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	PlayerUsableDebugHelper.Owner = this;
	
}

void APlayerUsable::DetachMeshFromPawn()
{

	Mesh1P->DetachFromParent();
	Mesh1P->SetHiddenInGame(true);

	Mesh3P->DetachFromParent();
	Mesh3P->SetHiddenInGame(true);

	
}


//////////////////////////////////////////////////////////////////////////
// Input

void APlayerUsable::Input_StartUse()
{
	PlayerUsableDebugHelper.FunctionName = "Input_StartUse()";
	PlayerUsableDebugHelper.PrintToConsole();

	StartUse();
}

void APlayerUsable::Input_StopUse()
{
	PlayerUsableDebugHelper.FunctionName = "Input_StopUse()";
	PlayerUsableDebugHelper.PrintToConsole();

	StopUse();
}

void APlayerUsable::Input_SwitchUseMode()
{
	SwitchUseMode();
}


void APlayerUsable::StartUse()
{
	if (Role != ROLE_Authority)
	{
		Server_StartUse();
		return;
	}
	

	PlayerUsableDebugHelper.FunctionName = "StartUse()";
	PlayerUsableDebugHelper.PrintToConsole();
	TrySetPlayerTryingToUse(true);
}

void APlayerUsable::StopUse()
{
	if (Role != ROLE_Authority)
	{
		Server_StopUse();
		return;
	}
	

	PlayerUsableDebugHelper.FunctionName = "StopUse()";
	PlayerUsableDebugHelper.PrintToConsole();
	TrySetPlayerTryingToUse(false);
	
}

void APlayerUsable::SwitchUseMode()
{
	
}

bool APlayerUsable::Server_StartUse_Validate()
{
	return true;
}

void APlayerUsable::Server_StartUse_Implementation()
{
	PlayerUsableDebugHelper.FunctionName = "Server_StartUse_Implementation()";
	PlayerUsableDebugHelper.PrintToConsole();
	StartUse();
}

bool APlayerUsable::Server_StopUse_Validate()
{
	return true;
}

void APlayerUsable::Server_StopUse_Implementation()
{
	PlayerUsableDebugHelper.FunctionName = "Server_StopUse_Implementation()";
	PlayerUsableDebugHelper.PrintToConsole();
	StopUse();
}

void APlayerUsable::Server_SwitchUseMode_Implementation()
{
	SwitchUseMode();
}

bool APlayerUsable::Server_SwitchUseMode_Validate()
{
	return true;
}

void APlayerUsable::OnEquip() 
{
	if (HasAuthority())
	{
		Client_OnEquip();
	}
}

void APlayerUsable::OnEquipFinished()
{
	if (HasAuthority())
	{
		Client_OnEquipFinished();
	}
}
void APlayerUsable::OnUnEquip() 
{
	if (HasAuthority())
	{
		Client_OnUnEquip();
	}
}

void APlayerUsable::OnUnEquipFinished()
{
	
}


void APlayerUsable::OnEnterInventory(AMercuryCharacter* NewOwner) 
{
	Client_OnEnterInventory(NewOwner);
	Server_OnEnterInventory(NewOwner);
	
}
void APlayerUsable::OnLeaveInventory() 
{
	Client_OnLeaveInventory();
	Server_OnLeaveInventory();
}




//////////////////////////////////////////////////////////////////////////
// Inventory Functions called by the client and run on client
void APlayerUsable::Client_OnEquip()
{
	AttachMeshToPawn();
	TrySetIsEquipped(true);
	
	
}

void APlayerUsable::Client_OnEquipFinished()
{

}
void APlayerUsable::Client_OnUnEquip()
{
	DetachMeshFromPawn();
	TrySetIsEquipped(false);
}

void APlayerUsable::Client_OnEnterInventory(AMercuryCharacter* NewOwner)
{
	TrySetIsOwned(true);
	Mesh1P->SetHiddenInGame(true);
	Mesh3P->SetHiddenInGame(true);
}


void APlayerUsable::Client_OnLeaveInventory()
{
	DetachMeshFromPawn();
	MyOwner = NULL;
	TrySetIsOwned(false);
	Mesh1P->SetHiddenInGame(true);
	Mesh3P->SetHiddenInGame(true);
}
//////////////////////////////////////////////////////////////////////////
// Animations

FItemAnim APlayerUsable::GetItemEquipAnim()
{
	return EquipAnim;
}

float APlayerUsable::PlayItemAnimation(const FItemAnim& Animation)
{


	float Duration = 0.0f;
	if (MyOwner)
	{
		UAnimMontage* AnimToUse = Animation.Montage1p;
		if (AnimToUse)
		{
			Duration = MyOwner->PlayAnimMontage(AnimToUse);
			//MyOwner->Multicast_PlayAnimMontage(AnimToUse);
		}
	}

	return Duration;
}

void APlayerUsable::StopItemAnimation(const FItemAnim& Animation)
{
	if (MyOwner)
	{
		UAnimMontage* UseAnim = Animation.Montage1p;
		if (UseAnim)
		{
			MyOwner->StopAnimMontage(UseAnim);
		}
	}
}


//////////////////////////////////////////////////////////////////////////
// Inventory Functions called on the server
void APlayerUsable::Server_OnEquip_Implementation()
{
	Client_OnEquip();
}
bool APlayerUsable::Server_OnEquip_Validate()
{
	return true;
}
void APlayerUsable::Server_OnUnEquip_Implementation()
{
	Client_OnUnEquip();
}
bool APlayerUsable::Server_OnUnEquip_Validate()
{
	return true;
}
void APlayerUsable::Server_OnEnterInventory_Implementation(class AMercuryCharacter* NewOwner)
{
	Client_OnEnterInventory(NewOwner);
}
bool APlayerUsable::Server_OnEnterInventory_Validate(class AMercuryCharacter* NewOwner)
{
	return true;
}
void APlayerUsable::Server_OnLeaveInventory_Implementation()
{
	Client_OnLeaveInventory();
}
bool APlayerUsable::Server_OnLeaveInventory_Validate()
{
	return true;

}
void APlayerUsable::Server_OnEquipFinished_Implementation()
{
	Client_OnEquipFinished();
}
bool APlayerUsable::Server_OnEquipFinished_Validate()
{
	return true;

}


// Interface for the PlayerUsable class
#pragma region Interface for PlayerUsable class

void APlayerUsable::Try_SwitchUseMode()
{
	if (bIsInSingleMode)
	{
		if (GetItemConfig().bCanBeBurst)
		{
			TrySetIsInSingleMode(false);
			TrySetIsInBurstMode(true);
		}
		else if (GetItemConfig().bCanBeAutomatic)
		{
			TrySetIsInSingleMode(false);
			TrySetIsInAutoMode(true);
		}
	}
	else if (bIsInBurstMode)
	{
		if (GetItemConfig().bCanBeAutomatic)
		{
			TrySetIsInBurstMode(false);
			TrySetIsInAutoMode(true);

		}
		else if (GetItemConfig().bCanBeSingle)
		{
			TrySetIsInBurstMode(false);
			TrySetIsInSingleMode(true);
		}
	}
	else if (bIsInAutomaticMode)
	{
		if (GetItemConfig().bCanBeSingle)
		{
			TrySetIsInAutoMode(false);
			TrySetIsInSingleMode(true);
		}
		else if (GetItemConfig().bCanBeBurst)
		{
			TrySetIsInAutoMode(false);
			TrySetIsInBurstMode(true);
		}
	}
	else
	{

	}

}

// Returns true if isAttached was set to newValue
void APlayerUsable::TrySetIsAttached(bool newValue)
{
	Client_TrySetIsAttached(newValue);
	Server_TrySetIsAttached(newValue);

}

// Returns true if isOwned was set to newValue
void APlayerUsable::TrySetIsOwned(bool newValue)
{
	Client_TrySetIsOwned(newValue);
	Server_TrySetIsOwned(newValue);

}

// Returns true if isEquipped was set  to newValue
void APlayerUsable::TrySetIsEquipped(bool newValue)
{
	Client_TrySetIsEquipped(newValue);
	Server_TrySetIsEquipped(newValue);

}

// Returns true if playerTryingToUseWasSet was set  to newValue
void APlayerUsable::TrySetPlayerTryingToUse(bool newValue)
{
	Client_TrySetPlayerTryingToUse(newValue);
	Server_TrySetPlayerTryingToUse(newValue);

}

// Returns true if isInAutomaticMode was set  to newValue
void APlayerUsable::TrySetIsInAutoMode(bool newValue)
{
	Client_TrySetIsInAutoMode(newValue);
	Server_TrySetIsInAutoMode(newValue);

}

// Returns true if IsInBurstMode was set  to newValue
void APlayerUsable::TrySetIsInBurstMode(bool newValue)
{
	Client_TrySetIsInBurstMode(newValue);
	Server_TrySetIsInBurstMode(newValue);

}

// Returns true if IsInBurstMode was set  to newValue
void APlayerUsable::TrySetIsInSingleMode(bool newValue)
{
	Client_TrySetIsInSingleMode(newValue);
	Server_TrySetIsInSingleMode(newValue);
}

#pragma endregion

// Client side functions. Currently only called from Authority
#pragma region Client Side Functions

void APlayerUsable::Client_TrySetIsOwned(bool newValue)
{
	bIsOwned = newValue;
}

void APlayerUsable::Client_TrySetIsAttached(bool newValue)
{
	bIsAttached = newValue;
}

void APlayerUsable::Client_TrySetIsEquipped(bool newValue)
{
	bIsEuipped = newValue;
}

void APlayerUsable::Client_TrySetPlayerTryingToUse(bool newValue)
{
	bPlayerTryingToUse = newValue;
}

void APlayerUsable::Client_TrySetIsInAutoMode(bool newValue)
{
	if (MyOwner)
	{
		if (newValue && GetItemConfig().bCanBeAutomatic)
		{
			bIsInSingleMode = false;
			bIsInBurstMode = false;
			bIsInAutomaticMode = true;
		}
		else
		{
			bIsInAutomaticMode = false;
		}
	}
	else
	{
		bIsInAutomaticMode = false;
	}
}

void APlayerUsable::Client_TrySetIsInBurstMode(bool newValue)
{
	if (MyOwner)
	{
		if (newValue && GetItemConfig().bCanBeBurst)
		{
			bIsInSingleMode = false;
			bIsInBurstMode = true;
			bIsInAutomaticMode = false;
		}
		else
		{
			bIsInBurstMode = false;
		}
	}
	else
	{
		bIsInBurstMode = false;
	}
}

void APlayerUsable::Client_TrySetIsInSingleMode(bool newValue)
{
	if (MyOwner)
	{
		if (newValue && GetItemConfig().bCanBeSingle)
		{
			bIsInSingleMode = true;
			bIsInBurstMode = false;
			bIsInAutomaticMode = false;
		}
		else
		{
			bIsInSingleMode = false;
		}
	}
	else
	{
		bIsInSingleMode = false;
	}
}



void APlayerUsable::Server_TrySetIsOwned_Implementation(bool newValue)
{
	Client_TrySetIsOwned(newValue);
}

bool APlayerUsable::Server_TrySetIsOwned_Validate(bool newValue)
{
	return true;

}

void APlayerUsable::Server_TrySetIsAttached_Implementation(bool newValue)
{
	Client_TrySetIsAttached(newValue);

}

bool APlayerUsable::Server_TrySetIsAttached_Validate(bool newValue)
{

	return true;
}

void APlayerUsable::Server_TrySetIsEquipped_Implementation(bool newValue)
{
	Client_TrySetIsEquipped(newValue);
}

bool APlayerUsable::Server_TrySetIsEquipped_Validate(bool newValue)
{
	return true;

}

void APlayerUsable::Server_TrySetPlayerTryingToUse_Implementation(bool newValue)
{
	Client_TrySetPlayerTryingToUse(newValue);
}

bool APlayerUsable::Server_TrySetPlayerTryingToUse_Validate(bool newValue)
{
	return true;

}

void APlayerUsable::Server_TrySetIsInAutoMode_Implementation(bool newValue)
{
	Client_TrySetIsInAutoMode(newValue);
}

bool APlayerUsable::Server_TrySetIsInAutoMode_Validate(bool newValue)
{

	return true;
}

void APlayerUsable::Server_TrySetIsInBurstMode_Implementation(bool newValue)
{
	Client_TrySetIsInBurstMode(newValue);
}

bool APlayerUsable::Server_TrySetIsInBurstMode_Validate(bool newValue)
{
	return true;

}

void APlayerUsable::Server_TrySetIsInSingleMode_Implementation(bool newValue)
{
	Client_TrySetIsInSingleMode(newValue);
}

bool APlayerUsable::Server_TrySetIsInSingleMode_Validate(bool newValue)
{
	return true;
}

// Theses functions return the state booleans
#pragma region State Getters

bool APlayerUsable::GetIsOwned()
{
	return bIsOwned;
}

bool APlayerUsable::GetIsAttached()
{
	return bIsAttached;
}

bool APlayerUsable::GetIsEquipped()
{
	return bIsEuipped;
}

bool APlayerUsable::GetPlayerTryingToUse()
{
	return bPlayerTryingToUse;
}

bool APlayerUsable::GetIsInAutoMode()
{
	return bIsInAutomaticMode;
}

bool APlayerUsable::GetIsInBurstMode()
{
	return bIsInBurstMode;
}

bool APlayerUsable::GetIsInSingleMode()
{
	return bIsInSingleMode;
}


#pragma endregion

#pragma endregion

