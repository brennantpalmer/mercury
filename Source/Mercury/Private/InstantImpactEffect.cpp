// Fill out your copyright notice in the Description page of Project Settings.

#include "Mercury.h"
#include "MercuryTypes.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterial.h"
#include "InstantImpactEffect.h"


// Sets default values
AInstantImpactEffect::AInstantImpactEffect()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bAutoDestroyWhenFinished = true;
}

// Called when the game starts or when spawned
void AInstantImpactEffect::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInstantImpactEffect::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AInstantImpactEffect::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	UPhysicalMaterial* HitPhysMat = SurfaceHit.PhysMaterial.Get();
	EPhysicalSurface HitSurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitPhysMat);

	// show particles
	UParticleSystem* ImpactFX = GetImpactFX(HitSurfaceType);
	if (ImpactFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, ImpactFX, GetActorLocation(), GetActorRotation());
	}

	//// play sound
	//USoundCue* ImpactSound = GetImpactSound(HitSurfaceType);
	//if (ImpactSound)
	//{
	//	UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, GetActorLocation());
	//}
}

UParticleSystem* AInstantImpactEffect::GetImpactFX(TEnumAsByte<EPhysicalSurface> SurfaceType) const
{
	UParticleSystem* ImpactFX = NULL;

	switch (SurfaceType)
	{
	case SHOOTER_SURFACE_Concrete:	ImpactFX = ConcreteFX; break;
	case SHOOTER_SURFACE_Dirt:		ImpactFX = DirtFX; break;
	case SHOOTER_SURFACE_Water:		ImpactFX = WaterFX; break;
	case SHOOTER_SURFACE_Metal:		ImpactFX = MetalFX; break;
	case SHOOTER_SURFACE_Wood:		ImpactFX = WoodFX; break;
	case SHOOTER_SURFACE_Grass:		ImpactFX = GrassFX; break;
	case SHOOTER_SURFACE_Glass:		ImpactFX = GlassFX; break;
	case SHOOTER_SURFACE_Flesh:		ImpactFX = FleshFX; break;
	default:						ImpactFX = DefaultFX; break;
	}

	return ImpactFX;
}

//USoundCue* AInstantImpactEffect::GetImpactSound(TEnumAsByte<EPhysicalSurface> SurfaceType) const
//{
//	USoundCue* ImpactSound = NULL;
//
//	switch (SurfaceType)
//	{
//	case SHOOTER_SURFACE_Concrete:	ImpactSound = ConcreteSound; break;
//	case SHOOTER_SURFACE_Dirt:		ImpactSound = DirtSound; break;
//	case SHOOTER_SURFACE_Water:		ImpactSound = WaterSound; break;
//	case SHOOTER_SURFACE_Metal:		ImpactSound = MetalSound; break;
//	case SHOOTER_SURFACE_Wood:		ImpactSound = WoodSound; break;
//	case SHOOTER_SURFACE_Grass:		ImpactSound = GrassSound; break;
//	case SHOOTER_SURFACE_Glass:		ImpactSound = GlassSound; break;
//	case SHOOTER_SURFACE_Flesh:		ImpactSound = FleshSound; break;
//	default:						ImpactSound = DefaultSound; break;
//	}
//
//	return ImpactSound;
//}