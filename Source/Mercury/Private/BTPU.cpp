// Fill out your copyright notice in the Description page of Project Settings.

#include "Mercury.h"
#include "PlayerUsable.h"
#include "BTPU.h"




/************************************************************************/
/* class PlayerUsableState member definitions                           */
/************************************************************************/

#pragma region PlayerUsableState Definitions

FPlayerUsableState::FPlayerUsableState()
{
	bIsAttached = false;
	bIsEuipped = false;
	bIsInSingleMode = true;
	bIsInBurstMode = false;
	bIsInAutomaticMode = false;
	bIsOwned = false;
}


// Interface for the PlayerUsable class
#pragma region Interface for PlayerUsable class

void FPlayerUsableState::Try_SwitchUseMode()
{
	if (bIsInSingleMode)
	{
		if (MyOwner->GetItemConfig().bCanBeBurst)
		{
			TrySetIsInSingleMode(false);
			TrySetIsInBurstMode(true);
		}
		else if (MyOwner->GetItemConfig().bCanBeAutomatic)
		{
			TrySetIsInSingleMode(false);
			TrySetIsInAutoMode(true);
		}
	}
	else if (bIsInBurstMode)
	{
		if (MyOwner->GetItemConfig().bCanBeAutomatic)
		{
			TrySetIsInBurstMode(false);
			TrySetIsInAutoMode(true);

		}
		else if (MyOwner->GetItemConfig().bCanBeSingle)
		{
			TrySetIsInBurstMode(false);
			TrySetIsInSingleMode(true);
		}
	}
	else if (bIsInAutomaticMode)
	{
		if (MyOwner->GetItemConfig().bCanBeSingle)
		{
			TrySetIsInAutoMode(false);
			TrySetIsInSingleMode(true);
		}
		else if (MyOwner->GetItemConfig().bCanBeBurst)
		{
			TrySetIsInAutoMode(false);
			TrySetIsInBurstMode(true);
		}
	}
	else
	{

	}

}

// Set Owning player usable class
void FPlayerUsableState::SetMyOwner(APlayerUsable* NewOwner)
{
	if (NewOwner)
	{
		MyOwner = NewOwner;
	}
}

// Returns true if isAttached was set to newValue
void FPlayerUsableState::TrySetIsAttached(bool newValue)
{
	Client_TrySetIsAttached(newValue);
}

// Returns true if isOwned was set to newValue
void FPlayerUsableState::TrySetIsOwned(bool newValue)
{
	Client_TrySetIsOwned(newValue);
}

// Returns true if isEquipped was set  to newValue
void FPlayerUsableState::TrySetIsEquipped(bool newValue)
{
	Client_TrySetIsEquipped(newValue);
}

// Returns true if playerTryingToUseWasSet was set  to newValue
void FPlayerUsableState::TrySetPlayerTryingToUse(bool newValue)
{
	Client_TrySetPlayerTryingToUse(newValue);
}

// Returns true if isInAutomaticMode was set  to newValue
void FPlayerUsableState::TrySetIsInAutoMode(bool newValue)
{
	Client_TrySetIsInAutoMode(newValue);
}

// Returns true if IsInBurstMode was set  to newValue
void FPlayerUsableState::TrySetIsInBurstMode(bool newValue)
{
	Client_TrySetIsInBurstMode(newValue);
}

// Returns true if IsInBurstMode was set  to newValue
void FPlayerUsableState::TrySetIsInSingleMode(bool newValue)
{
	Client_TrySetIsInSingleMode(newValue);
}

#pragma endregion

// Client side functions. Currently only called from Authority
#pragma region Client Side Functions

void FPlayerUsableState::Client_TrySetIsOwned(bool newValue)
{
	bIsOwned = newValue;
}

void FPlayerUsableState::Client_TrySetIsAttached(bool newValue)
{
	bIsAttached = newValue;
}

void FPlayerUsableState::Client_TrySetIsEquipped(bool newValue)
{
	bIsEuipped = newValue;
}

void FPlayerUsableState::Client_TrySetPlayerTryingToUse(bool newValue)
{
	bPlayerTryingToUse = newValue;
}

void FPlayerUsableState::Client_TrySetIsInAutoMode(bool newValue)
{
	if (MyOwner)
	{
		if (newValue && MyOwner->GetItemConfig().bCanBeAutomatic)
		{
			bIsInSingleMode = false;
			bIsInBurstMode = false;
			bIsInAutomaticMode = true;
		}
		else
		{
			bIsInAutomaticMode = false;
		}
	}
	else
	{
		bIsInAutomaticMode = false;
	}
}

void FPlayerUsableState::Client_TrySetIsInBurstMode(bool newValue)
{
	if (MyOwner)
	{
		if (newValue && MyOwner->GetItemConfig().bCanBeBurst)
		{
			bIsInSingleMode = false;
			bIsInBurstMode = true;
			bIsInAutomaticMode = false;
		}
		else
		{
			bIsInBurstMode = false;
		}
	}
	else
	{
		bIsInBurstMode = false;
	}
}

void FPlayerUsableState::Client_TrySetIsInSingleMode(bool newValue)
{
	if (MyOwner)
	{
		if (newValue && MyOwner->GetItemConfig().bCanBeSingle)
		{
			bIsInSingleMode = true;
			bIsInBurstMode = false;
			bIsInAutomaticMode = false;
		}
		else
		{
			bIsInSingleMode = false;
		}
	}
	else
	{
		bIsInSingleMode = false;
	}
}

// Theses functions return the state booleans
#pragma region State Getters

bool FPlayerUsableState::GetIsOwned()
{
	return bIsOwned;
}

bool FPlayerUsableState::GetIsAttached()
{
	return bIsAttached;
}

bool FPlayerUsableState::GetIsEquipped()
{
	return bIsEuipped;
}

bool FPlayerUsableState::GetPlayerTryingToUse()
{
	return bPlayerTryingToUse;
}

bool FPlayerUsableState::GetIsInAutoMode()
{
	return bIsInAutomaticMode;
}

bool FPlayerUsableState::GetIsInBurstMode()
{
	return bIsInBurstMode;
}

bool FPlayerUsableState::GetIsInSingleMode()
{
	return bIsInSingleMode;
}


#pragma endregion

#pragma endregion

#pragma endregion