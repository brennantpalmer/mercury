// Fill out your copyright notice in the Description page of Project Settings.

#include "Mercury.h"
#include "TestPlayerUsable.h"
#include "UnrealNetwork.h"
#include "MercuryCharacter.h"

//
//ATestPlayerUsable::ATestPlayerUsable()
//{
//	bPlayerIsTryingToUse = false;
//}
//
//
//void ATestPlayerUsable::PostInitializeComponents()
//{
//	Super::PostInitializeComponents();
//}
//
//void ATestPlayerUsable::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
//{
//	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
//	DOREPLIFETIME(ATestPlayerUsable, bPlayerIsTryingToUse);
//
//}
//
//// Called when the game starts or when spawned
//void ATestPlayerUsable::BeginPlay()
//{
//	Super::BeginPlay();
//
//}
//
//// Called every frame
//void ATestPlayerUsable::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//
//}
//
///////////////////////////////////////////////////////////////////////////////
//// Player Interface
//
//void ATestPlayerUsable::StartUse()
//{
//	if (Role == ROLE_Authority)
//	{
//		Server_Fire();
//	}
//}
//
//void ATestPlayerUsable::StopUse()
//{
//
//}
//
////////////////////////////////////////////////////////////////////////////
//// Inventory Functions called by the client and run on client
//void ATestPlayerUsable::OnEquip()
//{
//	AttachMeshToPawn();
//}
//
//void ATestPlayerUsable::OnUnEquip()
//{
//	DetachMeshFromPawn();
//}
//
//void ATestPlayerUsable::OnEnterInventory(AMercuryCharacter* NewOwner)
//{
//	Mesh1P->SetHiddenInGame(true);
//	Mesh3P->SetHiddenInGame(true);
//}
//
//
//void ATestPlayerUsable::OnLeaveInventory()
//{
//	DetachMeshFromPawn();
//	MyOwner = NULL;
//	Mesh1P->SetHiddenInGame(true);
//	Mesh3P->SetHiddenInGame(true);
//}
//
//void ATestPlayerUsable::AttachMeshToPawn()
//{
//	if (MyOwner)
//	{
//
//		// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
//
//		static FName AttachPoint(TEXT("RightHand"));
//		USkeletalMeshComponent* PawnMesh3p = MyOwner->GetMesh();
//		Mesh1P->SetHiddenInGame(true);
//		Mesh3P->SetHiddenInGame(false);
//		Mesh1P->AttachTo(PawnMesh3p, AttachPoint);
//		Mesh3P->AttachTo(PawnMesh3p, AttachPoint);
//
//		AttachRootComponentTo(MyOwner->GetMesh(), AttachPoint, EAttachLocation::SnapToTarget, true);
//	}
//}
//
//void ATestPlayerUsable::DetachMeshFromPawn()
//{
//
//	Mesh1P->DetachFromParent();
//	Mesh1P->SetHiddenInGame(true);
//
//	Mesh3P->DetachFromParent();
//	Mesh3P->SetHiddenInGame(true);
//
//
//}
//
//void ATestPlayerUsable::Fire()
//{
//	
//	const int32 RandomSeed = FMath::Rand();
//	FRandomStream WeaponRandomStream(RandomSeed);
//	const float CurrentSpread = GetCurrentSpread();
//	const float ConeHalfAngle = FMath::DegreesToRadians(CurrentSpread * 0.5f);
//
//	const FVector AimDir = GetAdjustedAim();
//	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
//	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
//	const FVector EndTrace = StartTrace + ShootDir * InstantConfig.WeaponRange;
//
//	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
//	ProcessInstantHit(Impact, StartTrace, ShootDir, RandomSeed, CurrentSpread);
//
//	CurrentFiringSpread = FMath::Min(InstantConfig.FiringSpreadMax, CurrentFiringSpread + InstantConfig.FiringSpreadIncrement);
//
//	//GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &AInstantWeapon::FireWeapon, ItemConfig.UseInterval, false);
//
//}
//
//void ATestPlayerUsable::Server_Fire_Implementation()
//{
//	Fire();
//}
//
//bool ATestPlayerUsable::Server_Fire_Validate()
//{
//	return true;
//}
//
//
////////////////////////////////////////////////////////////////////////////
//// Weapon usage helpers
//
//float ATestPlayerUsable::GetCurrentSpread() const
//{
//	float FinalSpread = InstantConfig.WeaponSpread + CurrentFiringSpread;
//	if (MyOwner && MyOwner->IsInADS())
//	{
//		FinalSpread *= InstantConfig.TargetingSpreadMod;
//	}
//
//	return FinalSpread;
//}
//
//FVector ATestPlayerUsable::GetCameraDamageStartLocation(const FVector& AimDir) const
//{
//	APlayerController* PC = MyOwner ? Cast<APlayerController>(MyOwner->Controller) : NULL;
//	//	AAIController* AIPC = MyOwner ? Cast<AAIController>(MyOwner->Controller) : NULL;
//	FVector OutStartTrace = FVector::ZeroVector;
//
//	if (PC)
//	{
//		// use player's camera
//		FRotator UnusedRot;
//		PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);
//
//		// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
//		OutStartTrace = OutStartTrace + AimDir * ((Instigator->GetActorLocation() - OutStartTrace) | AimDir);
//	}
//	/*else if (AIPC)
//	{
//	OutStartTrace = GetMuzzleLocation();
//	}*/
//
//	return OutStartTrace;
//}