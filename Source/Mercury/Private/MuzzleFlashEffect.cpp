// Fill out your copyright notice in the Description page of Project Settings.

#include "Mercury.h"
#include "MuzzleFlashEffect.h"


// Sets default values
AMuzzleFlashEffect::AMuzzleFlashEffect()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bAutoDestroyWhenFinished = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void AMuzzleFlashEffect::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMuzzleFlashEffect::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


void AMuzzleFlashEffect::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
	if (DefaultFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, DefaultFX, GetActorLocation(), GetActorRotation());
	}
}
