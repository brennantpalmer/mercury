// Fill out your copyright notice in the Description page of Project Settings.

#include "Mercury.h"
#include "InstantWeapon.h"
#include "InstantImpactEffect.h"
#include "MercuryCharacter.h"
#include "UnrealNetwork.h"
#include "MuzzleFlashEffect.h"
#include "BTPU.h"

AInstantWeapon::AInstantWeapon()
{
	InstantWeaponDebugHelper.ClassName = "AInstantWeapon";
	InstantWeaponDebugHelper.bShouldPrint = false;


	MuzzleAttachPoint = "MuzzleFlash";
	CurrentFiringSpread = 0.0f;
	bAlwaysRelevant = true;
	bNetLoadOnClient = true;
	bReplicates = true;

}

void AInstantWeapon::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

// Called when the game starts or when spawned
void AInstantWeapon::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AInstantWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInstantWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	PlayerUsableDebugHelper.Owner = this;
}

void AInstantWeapon::Input_StartUse()
{
	InstantWeaponDebugHelper.FunctionName = "Input_StartUse()";
	InstantWeaponDebugHelper.PrintToConsole();
	StartUse();
}
void AInstantWeapon::Input_StopUse()
{
	InstantWeaponDebugHelper.FunctionName = "Input_StopUse()";
	InstantWeaponDebugHelper.PrintToConsole();
	StopUse();
}


void AInstantWeapon::StartUse()
{

	if (!HasAuthority())
	{
		Server_StartUse();
		return;
	}

	InstantWeaponDebugHelper.FunctionName = "StartUse()";
	InstantWeaponDebugHelper.PrintToConsole();

	Super::StartUse();
	FireWeapon();

}

void AInstantWeapon::StopUse()
{
	Super::StopUse();
}

void AInstantWeapon::SwitchUseMode()
{
	Super::SwitchUseMode();
}

bool AInstantWeapon::Server_StartUse_Validate()
{
	return true;
}

void AInstantWeapon::Server_StartUse_Implementation()
{
	StartUse();
}

bool AInstantWeapon::Server_StopUse_Validate()
{
	return true;
}

void AInstantWeapon::Server_StopUse_Implementation()
{
	StopUse();
}

void AInstantWeapon::Server_SwitchUseMode_Implementation()
{
	SwitchUseMode();
}

bool AInstantWeapon::Server_SwitchUseMode_Validate()
{
	return true;
}

void AInstantWeapon::SimulateInstantHit(const FVector& ShotOrigin, int32 RandomSeed, float ReticleSpread)
{
	InstantWeaponDebugHelper.FunctionName = "SimulateInstantHit(const FVector& ShotOrigin, int32 RandomSeed, float ReticleSpread)";
	InstantWeaponDebugHelper.PrintToConsole();

	FRandomStream WeaponRandomStream(RandomSeed);
	const float ConeHalfAngle = FMath::DegreesToRadians(ReticleSpread * 0.5f);

	const FVector StartTrace = ShotOrigin;
	const FVector AimDir = GetAdjustedAim();
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
	const FVector EndTrace = StartTrace + ShootDir * InstantConfig.WeaponRange;

	FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
	if (Impact.bBlockingHit)
	{
		SpawnImpactEffects(Impact);
		SpawnTrailEffect(Impact.ImpactPoint);
	}
	else
	{
		SpawnTrailEffect(EndTrace);
	}
}

void AInstantWeapon::SpawnImpactEffects(const FHitResult& Impact)
{
	InstantWeaponDebugHelper.FunctionName = "SpawnImpactEffects(const FHitResult& Impact)";
	InstantWeaponDebugHelper.PrintToConsole();

	if (ImpactTemplate && Impact.bBlockingHit)
	{
		InstantWeaponDebugHelper.FunctionName = "SpawnImpactEffects(const FHitResult& Impact)";
		InstantWeaponDebugHelper.PrintToConsole("BlockingHit && ImpactTemplate Valid");

		FHitResult UseImpact = Impact;

		// trace again to find component lost during replication
		if (!Impact.Component.IsValid())
		{
			const FVector StartTrace = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;
			const FVector EndTrace = Impact.ImpactPoint - Impact.ImpactNormal * 10.0f;
			FHitResult Hit = WeaponTrace(StartTrace, EndTrace);
			UseImpact = Hit;
		}

		FTransform const SpawnTransform(Impact.ImpactNormal.Rotation(), Impact.ImpactPoint);
		AInstantImpactEffect* EffectActor = GetWorld()->SpawnActorDeferred<AInstantImpactEffect>(ImpactTemplate, SpawnTransform);
		if (EffectActor)
		{
			EffectActor->SurfaceHit = UseImpact;
			UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		}
		else
		{
			InstantWeaponDebugHelper.FunctionName = "SpawnImpactEffects(const FHitResult& Impact)";
			InstantWeaponDebugHelper.PrintToConsole("EffectActor Invalid");
		}
	}
	else
	{
		InstantWeaponDebugHelper.FunctionName = "SpawnImpactEffects(const FHitResult& Impact)";
		InstantWeaponDebugHelper.PrintToConsole("BlockingHit || ImpactTemplate inValid");
	}
}

void AInstantWeapon::SpawnTrailEffect(const FVector& EndPoint)
{
	if (TrailFX)
	{
		const FVector Origin = GetMuzzleLocation();

		UParticleSystemComponent* TrailPSC = UGameplayStatics::SpawnEmitterAtLocation(this, TrailFX, Origin);
		if (TrailPSC)
		{
			TrailPSC->SetVectorParameter(TrailTargetParam, EndPoint);
		}
	}
}

FVector AInstantWeapon::GetAdjustedAim() const
{
	APlayerController* const PlayerController = Instigator ? Cast<APlayerController>(Instigator->Controller) : NULL;
	FVector FinalAim = FVector::ZeroVector;
	// If we have a player controller use it for the aim
	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}

	return FinalAim;
}

FVector AInstantWeapon::GetMuzzleLocation() const
{
	USkeletalMeshComponent* UseMesh = Mesh3P;
	return UseMesh->GetSocketLocation(MuzzleAttachPoint);
}

FVector AInstantWeapon::GetMuzzleDirection() const
{
	USkeletalMeshComponent* UseMesh = Mesh3P;
	return UseMesh->GetSocketRotation(MuzzleAttachPoint).Vector();
}

FHitResult AInstantWeapon::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));

	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, COLLISION_WEAPON, TraceParams);

	return Hit;
}

//////////////////////////////////////////////////////////////////////////
// Weapon usage helpers

float AInstantWeapon::GetCurrentSpread() const
{
	float FinalSpread = InstantConfig.WeaponSpread + CurrentFiringSpread;
	if (MyOwner && MyOwner->IsInADS())
	{
		FinalSpread *= InstantConfig.TargetingSpreadMod;
	}

	return FinalSpread;
}

FVector AInstantWeapon::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	APlayerController* PC = MyOwner ? Cast<APlayerController>(MyOwner->Controller) : NULL;
//	AAIController* AIPC = MyOwner ? Cast<AAIController>(MyOwner->Controller) : NULL;
	FVector OutStartTrace = FVector::ZeroVector;

	if (PC)
	{
		// use player's camera
		FRotator UnusedRot;
		PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);

		// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
		OutStartTrace = OutStartTrace + AimDir * ((Instigator->GetActorLocation() - OutStartTrace) | AimDir);
	}
	/*else if (AIPC)
	{
		OutStartTrace = GetMuzzleLocation();
	}*/

	return OutStartTrace;
}

//////////////////////////////////////////////////////////////////////////
// Weapon usage

void AInstantWeapon::FireWeapon()
{
	InstantWeaponDebugHelper.FunctionName = "FireWeapon()";
	InstantWeaponDebugHelper.PrintToConsole();
	
	if (!GetPlayerTryingToUse() || !HasAuthority())
	{
		InstantWeaponDebugHelper.PrintToConsole("Weapon Fire Stopped");
		return;
	}

	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpread = GetCurrentSpread();
	const float ConeHalfAngle = FMath::DegreesToRadians(CurrentSpread * 0.5f);

	const FVector AimDir = GetAdjustedAim();
	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
	const FVector EndTrace = StartTrace + ShootDir * InstantConfig.WeaponRange;

	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
	ProcessInstantHit(Impact, StartTrace, ShootDir, RandomSeed, CurrentSpread);

	Multicast_SimulateWeaponFire();
	InstantWeaponDebugHelper.FunctionName = "FireWeapon()";
	InstantWeaponDebugHelper.PrintToConsole("Weapon Fired");

	CurrentFiringSpread = FMath::Min(InstantConfig.FiringSpreadMax, CurrentFiringSpread + InstantConfig.FiringSpreadIncrement);

	GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &AInstantWeapon::FireWeapon, ItemConfig.UseInterval, false);

}

bool AInstantWeapon::ServerNotifyHit_Validate(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)
{
	return true;
}

void AInstantWeapon::ServerNotifyHit_Implementation(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)
{
	InstantWeaponDebugHelper.FunctionName = "ServerNotifyHit_Implementation(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)";
	InstantWeaponDebugHelper.PrintToConsole();

	const float WeaponAngleDot = FMath::Abs(FMath::Sin(ReticleSpread * PI / 180.f));

	// if we have an instigator, calculate dot between the view and the shot
	if (Instigator && (Impact.GetActor() || Impact.bBlockingHit))
	{
		const FVector Origin = GetMuzzleLocation();
		const FVector ViewDir = (Impact.Location - Origin).GetSafeNormal();

		// is the angle between the hit and the view within allowed limits (limit + weapon max angle)
		const float ViewDotHitDir = FVector::DotProduct(Instigator->GetViewRotation().Vector(), ViewDir);
		if (ViewDotHitDir > InstantConfig.AllowedViewDotHitDir - WeaponAngleDot)
		{
			if (true)
			{
				if (Impact.GetActor() == NULL)
				{
					if (Impact.bBlockingHit)
					{
						ProcessInstantHit_Confirmed(Impact, Origin, ShootDir, RandomSeed, ReticleSpread);
					}
				}
				// assume it told the truth about static things because the don't move and the hit 
				// usually doesn't have significant gameplay implications
				else if (Impact.GetActor()->IsRootComponentStatic() || Impact.GetActor()->IsRootComponentStationary())
				{
					ProcessInstantHit_Confirmed(Impact, Origin, ShootDir, RandomSeed, ReticleSpread);
				}
				else
				{
					// Get the component bounding box
					const FBox HitBox = Impact.GetActor()->GetComponentsBoundingBox();

					// calculate the box extent, and increase by a leeway
					FVector BoxExtent = 0.5 * (HitBox.Max - HitBox.Min);
					BoxExtent *= InstantConfig.ClientSideHitLeeway;

					// avoid precision errors with really thin objects
					BoxExtent.X = FMath::Max(20.0f, BoxExtent.X);
					BoxExtent.Y = FMath::Max(20.0f, BoxExtent.Y);
					BoxExtent.Z = FMath::Max(20.0f, BoxExtent.Z);

					// Get the box center
					const FVector BoxCenter = (HitBox.Min + HitBox.Max) * 0.5;

					// if we are within client tolerance
					if (FMath::Abs(Impact.Location.Z - BoxCenter.Z) < BoxExtent.Z &&
						FMath::Abs(Impact.Location.X - BoxCenter.X) < BoxExtent.X &&
						FMath::Abs(Impact.Location.Y - BoxCenter.Y) < BoxExtent.Y)
					{
						ProcessInstantHit_Confirmed(Impact, Origin, ShootDir, RandomSeed, ReticleSpread);
					}
					else
					{
						UE_LOG(LogTemp, Warning, TEXT("%s Rejected client side hit of %s (outside bounding box tolerance)"), *GetNameSafe(this), *GetNameSafe(Impact.GetActor()));
					}
				}
			}
		}
		else if (ViewDotHitDir <= InstantConfig.AllowedViewDotHitDir)
		{
			UE_LOG(LogTemp, Warning, TEXT("%s Rejected client side hit of %s (facing too far from the hit direction)"), *GetNameSafe(this), *GetNameSafe(Impact.GetActor()));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("%s Rejected client side hit of %s"), *GetNameSafe(this), *GetNameSafe(Impact.GetActor()));
		}
	}
}

bool AInstantWeapon::ServerNotifyMiss_Validate(FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)
{
	return true;
}

void AInstantWeapon::ServerNotifyMiss_Implementation(FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)
{
	InstantWeaponDebugHelper.FunctionName = "ServerNotifyMiss_Implementation(FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)";
	InstantWeaponDebugHelper.PrintToConsole();

	const FVector Origin = GetMuzzleLocation();
	const FVector EndTrace = Origin + ShootDir * InstantConfig.WeaponRange;
	SpawnTrailEffect(EndTrace);
}

void AInstantWeapon::ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread)
{

	InstantWeaponDebugHelper.FunctionName = "ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread)";
	InstantWeaponDebugHelper.PrintToConsole();

	if (MyOwner && MyOwner->IsLocallyControlled() && GetNetMode() == NM_Client)
	{
		// if we're a client and we've hit something that is being controlled by the server
		if (Impact.GetActor() && Impact.GetActor()->GetRemoteRole() == ROLE_Authority)
		{
			// notify the server of the hit
			ServerNotifyHit(Impact, ShootDir, RandomSeed, ReticleSpread);
		}
		else if (Impact.GetActor() == NULL)
		{
			if (Impact.bBlockingHit)
			{
				// notify the server of the hit
				ServerNotifyHit(Impact, ShootDir, RandomSeed, ReticleSpread);
			}
			else
			{
				// notify server of the miss
				ServerNotifyMiss(ShootDir, RandomSeed, ReticleSpread);
			}
		}
	}

	// process a confirmed hit
	ProcessInstantHit_Confirmed(Impact, Origin, ShootDir, RandomSeed, ReticleSpread);
}

void AInstantWeapon::ProcessInstantHit_Confirmed(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread)
{

	if (!HasAuthority())
	{
		return;
	}

	InstantWeaponDebugHelper.FunctionName = "ProcessInstantHit_Confirmed(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread)";
	InstantWeaponDebugHelper.PrintToConsole();

	// handle damage
	if (ShouldDealDamage(Impact.GetActor()))
	{
		DealDamage(Impact, ShootDir);
	}

	Multicast_SimulateInstantHit(Origin, RandomSeed, ReticleSpread);
	
}

bool AInstantWeapon::ShouldDealDamage(AActor* TestActor) const
{
	/*InstantWeaponDebugHelper.FunctionName = "ShouldDealDamage(AActor* TestActor)";
	InstantWeaponDebugHelper.PrintToConsole();*/

	// if we're an actor on the server, or the actor's role is authoritative, we should register damage
	if (TestActor)
	{
		if (GetNetMode() != NM_Client ||
			TestActor->Role == ROLE_Authority ||
			TestActor->bTearOff)
		{
			return true;
		}
	}

	return false;
}

void AInstantWeapon::DealDamage(const FHitResult& Impact, const FVector& ShootDir)
{
	InstantWeaponDebugHelper.FunctionName = "DealDamage(const FHitResult& Impact, const FVector& ShootDir)";
	InstantWeaponDebugHelper.PrintToConsole();

	FPointDamageEvent PointDmg;
	PointDmg.DamageTypeClass = InstantConfig.DamageType;
	PointDmg.HitInfo = Impact;
	PointDmg.ShotDirection = ShootDir;
	PointDmg.Damage = InstantConfig.HitDamage;



	Impact.GetActor()->TakeDamage(PointDmg.Damage, PointDmg, MyOwner->Controller, this);
}

void AInstantWeapon::OnBurstFinished()
{
//	Super::OnBurstFinished();

	CurrentFiringSpread = 0.0f;
}

void  AInstantWeapon::Multicast_SimulateInstantHit_Implementation(const FVector& Origin, int32 RandomSeed, float ReticleSpread)
{
	
	SimulateInstantHit(Origin, RandomSeed, ReticleSpread);
}

void AInstantWeapon::Multicast_SimulateWeaponFire_Implementation()
{
	SimulateWeaponFire();
}

void AInstantWeapon::SimulateWeaponFire()
{
	/*if (Role == ROLE_Authority)
	{
		return;
	}*/
		
	if (MuzzleFlashTemplate)
	{
		USkeletalMeshComponent* UseWeaponMesh = Mesh3P;
		FTransform const SpawnTransform = Mesh3P->GetSocketTransform(MuzzleAttachPoint);
		AMuzzleFlashEffect* EffectActor = GetWorld()->SpawnActorDeferred<AMuzzleFlashEffect>(MuzzleFlashTemplate, SpawnTransform);
		if (EffectActor)
		{
			UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		}
		else
		{
			InstantWeaponDebugHelper.FunctionName = "SpawnImpactEffects(const FHitResult& Impact)";
			InstantWeaponDebugHelper.PrintToConsole("EffectActor Invalid");
		}

		//if (!bLoopedMuzzleFX || MuzzlePSC == NULL)
		//{
		//	// Split screen requires we create 2 effects. One that we see and one that the other player sees.
		//	if ((MyOwner != NULL))
		//	{
		//		AController* PlayerCon = MyOwner->GetController();
		//		if (PlayerCon != NULL)
		//		{
		//			Mesh3P->GetSocketLocation(MuzzleAttachPoint);
		//			MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, Mesh1P, MuzzleAttachPoint);
		//			if (MuzzlePSC)
		//			{
		//				MuzzlePSC->bOwnerNoSee = false;
		//				MuzzlePSC->bOnlyOwnerSee = false;
		//			}
		//		
		//	
		//		}
		//	}
		//	else
		//	{
		//		MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, UseWeaponMesh, MuzzleAttachPoint);
		//	}
	}
	else
	{
		InstantWeaponDebugHelper.FunctionName = "void SimulateWeaponFire()";
		InstantWeaponDebugHelper.PrintToConsole("MuzzleFX == NULL");
	}

	PlayItemAnimation(FireAnim);

	if (bLoopedFireSound)
	{
		if (FireAC == NULL)
		{
			FireAC = PlayItemSound(FireLoopSound);
		}
	}
	else
	{
		PlayItemSound(FireSound);
	}

}

void AInstantWeapon::StopSimulatingWeaponFire()
{
	if (bLoopedMuzzleFX)
	{
		if (MuzzlePSC != NULL)
		{
			MuzzlePSC->DeactivateSystem();
			MuzzlePSC = NULL;
		}
	}

	if (bLoopedFireAnim && bPlayingFireAnim)
	{
		StopItemAnimation(FireAnim);
		bPlayingFireAnim = false;
	}

	if (FireAC)
	{
		FireAC->FadeOut(0.1f, 0.0f);
		FireAC = NULL;

		PlayItemSound(FireFinishSound);
	}
}


UAudioComponent* APlayerUsable::PlayItemSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound && MyOwner)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, MyOwner->GetRootComponent());
	}

	return AC;
}