// Fill out your copyright notice in the Description page of Project Settings.
// Test class to get down to the bottom of what is going wrong with the replication 


#pragma once

#include "GameFramework/Actor.h"
#include "Mercury.h"
//#include "TestPlayerUsable.generated.h"

//USTRUCT()
//struct FTestInstantHitInfo
//{
//	GENERATED_USTRUCT_BODY()
//
//		UPROPERTY()
//		FVector Origin;
//
//	UPROPERTY()
//		float ReticleSpread;
//
//	UPROPERTY()
//		int32 RandomSeed;
//};
//
//USTRUCT()
//struct FTestInstantWeaponData
//{
//	GENERATED_USTRUCT_BODY()
//
//		/** base weapon spread (degrees) */
//		UPROPERTY(EditDefaultsOnly, Category = Accuracy)
//		float WeaponSpread;
//
//	/** targeting spread modifier */
//	UPROPERTY(EditDefaultsOnly, Category = Accuracy)
//		float TargetingSpreadMod;
//
//	/** continuous firing: spread increment */
//	UPROPERTY(EditDefaultsOnly, Category = Accuracy)
//		float FiringSpreadIncrement;
//
//	/** continuous firing: max increment */
//	UPROPERTY(EditDefaultsOnly, Category = Accuracy)
//		float FiringSpreadMax;
//
//	/** weapon range */
//	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
//		float WeaponRange;
//
//	/** damage amount */
//	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
//		int32 HitDamage;
//
//	/** type of damage */
//	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
//		TSubclassOf<UDamageType> DamageType;
//
//	/** hit verification: scale for bounding box of hit actor */
//	UPROPERTY(EditDefaultsOnly, Category = HitVerification)
//		float ClientSideHitLeeway;
//
//	/** hit verification: threshold for dot product between view direction and hit direction */
//	UPROPERTY(EditDefaultsOnly, Category = HitVerification)
//		float AllowedViewDotHitDir;
//
//	/** defaults */
//	FInstantWeaponData()
//	{
//		WeaponSpread = 5.0f;
//		TargetingSpreadMod = 0.25f;
//		FiringSpreadIncrement = 1.0f;
//		FiringSpreadMax = 10.0f;
//		WeaponRange = 10000.0f;
//		HitDamage = 10;
//		DamageType = UDamageType::StaticClass();
//		ClientSideHitLeeway = 200.0f;
//		AllowedViewDotHitDir = 0.8f;
//	}
//};
//
//UCLASS()
//class ATestPlayerUsable : public AActor
//{
//
//	GENERATED_BODY()
//
//protected:
//	bool bPlayerIsTryingToUse;
//	class AMercuryCharacter* MyOwner;
//
//	/** current spread from continuous firing */
//	float CurrentFiringSpread;
//
//public:
//
//	// Called when the game starts or when spawned
//	virtual void BeginPlay() override;
//
//	// Called every frame
//	virtual void Tick(float DeltaSeconds) override;
//
//	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
//
//	/** setup initial variables */
//	virtual void PostInitializeComponents() override;
//
//public:
//	ATestPlayerUsable();
//
//	/////////////////////////////////////////////////////////////////////////
//	// Meshes
//
//#pragma region Skeletal Meshes
//
//protected:
//	/** weapon mesh: 1st person view */
//	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
//		USkeletalMeshComponent* Mesh1P;
//
//	/** weapon mesh: 3rd person view */
//	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
//		USkeletalMeshComponent* Mesh3P;
//
//
//#pragma endregion
//
//
//	//////////////////////////////////////////////////////////////////////////
//	// Inventory
//
//#pragma region Inventory
//
//	/** attaches items mesh to pawn's mesh */
//	void AttachMeshToPawn();
//
//	/** detaches items mesh from pawn */
//	void DetachMeshFromPawn();
//
//	virtual void OnEquip();
//	virtual void OnUnEquip();
//
//	void OnEnterInventory(class AMercuryCharacter* NewOwner);
//	void OnLeaveInventory();
//
//#pragma endregion
//
//#pragma region Player Interface
//	
//	virtual void StartUse();
//	virtual void StopUse();
//
//#pragma endregion
//
//	virtual void Fire();
//
//	UFUNCTION(Server, Reliable, WithValidation)
//	void Server_Fire();
//
//	// Weapon usage helpers
//	/** get the originating location for camera damage */
//	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;
//
//
//};