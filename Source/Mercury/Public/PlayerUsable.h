// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BTPU.h"
#include "Runtime/Engine/Classes/Sound/SoundCue.h"
#include "PlayerUsable.generated.h"



USTRUCT()
struct FItemAnim
{
	GENERATED_BODY()

	/** animation played on pawn (1st person view) */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* Montage1p;

	/** animation played on pawn (3rd person view) */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* Montage3p;
};

USTRUCT()
struct FItemConfig
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Item)
		FString ItemName;


	// if true, then item can be set to continuously be used at the UseInterval
	UPROPERTY(EditDefaultsOnly, Category = Item)
		bool bCanBeAutomatic;

	// if true, then item can be set to be used once and then stop. UseInterval determines when the item is able to be used again
	UPROPERTY(EditDefaultsOnly, Category = Item)
		bool bCanBeSingle;

	// if true, then item can be used in bursts. UseInterval determines the interval between the bursts and the minimum time until can be burst again
	UPROPERTY(EditDefaultsOnly, Category = Item)
		bool bCanBeBurst;

	/** interval between Use calls when the player is holding down the use button. */
	UPROPERTY(EditDefaultsOnly, Category = Item)
		float UseInterval;

	// Defaults
	FItemConfig()
		:
		bCanBeAutomatic(false),
		bCanBeBurst(false),
		bCanBeSingle(true),
		ItemName("Default"),
		UseInterval(.2f)
	{}

};

UCLASS()
class MERCURY_API APlayerUsable : public AActor
{
	GENERATED_BODY()
	

#pragma region PlayerUsable State vars and functions

#pragma region Boolean States

private:

	UPROPERTY(Replicated)
	bool bIsOwned;
	UPROPERTY(Replicated)
	bool bIsAttached;
	UPROPERTY(Replicated)
	bool bIsEuipped;
	UPROPERTY(Replicated)
	bool bPlayerTryingToUse;
	UPROPERTY(Replicated)
	bool bIsInAutomaticMode;
	UPROPERTY(Replicated)
	bool bIsInBurstMode;
	UPROPERTY(Replicated)
	bool bIsInSingleMode;

#pragma endregion

#pragma region State Setters 

protected:

	// When called, switched the items use mode (Single, Burst, Auto) based on what the current mode is
	void Try_SwitchUseMode();

	void TrySetIsAttached(bool newValue);
	void TrySetIsOwned(bool newValue);
	void TrySetIsEquipped(bool newValue);
	void TrySetPlayerTryingToUse(bool newValue);
	void TrySetIsInAutoMode(bool newValue);
	void TrySetIsInBurstMode(bool newValue);
	void TrySetIsInSingleMode(bool newValue);

#pragma endregion

#pragma region Client State Setters

	void Client_TrySetIsOwned(bool newValue);
	void Client_TrySetIsAttached(bool newValue);
	void Client_TrySetIsEquipped(bool newValue);
	void Client_TrySetPlayerTryingToUse(bool newValue);
	void Client_TrySetIsInAutoMode(bool newValue);
	void Client_TrySetIsInBurstMode(bool newValue);
	void Client_TrySetIsInSingleMode(bool newValue);

#pragma endregion

#pragma region State Setters

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_TrySetIsOwned(bool newValue);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_TrySetIsAttached(bool newValue);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_TrySetIsEquipped(bool newValue);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_TrySetPlayerTryingToUse(bool newValue);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_TrySetIsInAutoMode(bool newValue);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_TrySetIsInBurstMode(bool newValue);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_TrySetIsInSingleMode(bool newValue);

#pragma endregion

	// Theses functions return the state booleans
#pragma region State Getters

public:

	bool GetIsOwned();
	bool GetIsAttached();
	bool GetIsEquipped();
	bool GetPlayerTryingToUse();
	bool GetIsInAutoMode();
	bool GetIsInBurstMode();
	bool GetIsInSingleMode();

#pragma endregion

#pragma endregion


public:	
	APlayerUsable();

	DebugHelper PlayerUsableDebugHelper;

#pragma region AActor Overides

public:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;

	/** setup initial variables */
	virtual void PostInitializeComponents() override;

#pragma endregion

#pragma region Skeletal Meshes

protected:
	/** weapon mesh: 1st person view */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* Mesh1P;

	/** weapon mesh: 3rd person view */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* Mesh3P;


#pragma endregion

protected:
	/** pawn owner */
	UPROPERTY(Transient, Replicated)
	class AMercuryCharacter* MyOwner;


public:

	const class AMercuryCharacter* GetMyOwner();
	void SetMyOwner(class AMercuryCharacter* newOwner);

	// Timer handles for the class
#pragma region Timer Handles

	/** Handle for efficient management of OnEquipFinished timer */
	FTimerHandle TimerHandle_OnEquipFinished;

	/** Handle for efficient management of HandleFiring timer */
	FTimerHandle TimerHandle_HandleFiring;
	
#pragma endregion

	// Input
	// Functions called by owning pawn
#pragma region Input from Pawn

public:

	virtual void Input_StartUse();
	virtual void Input_StopUse();

	// When called, switched the items use mode (Single, Burst, Auto) based on what the current mode is
	virtual void Input_SwitchUseMode();

#pragma endregion

#pragma region Client and Server functions that are called by the input functions


protected:
	
	// Input Handlers (client + server)

#pragma region Client Functions

	virtual void StartUse();

	virtual void StopUse();

	virtual void SwitchUseMode();

#pragma endregion

	// Input - server side

#pragma region Server Functions

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_StartUse();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_StopUse();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SwitchUseMode();

#pragma endregion

#pragma endregion

protected:

	/** firing audio (bLoopedFireSound set) */
	UPROPERTY(Transient)
		UAudioComponent* FireAC;

	UPROPERTY(EditDefaultsOnly, Category = Config)
		FItemConfig ItemConfig;
	
	/** equip animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FItemAnim EquipAnim;

	/** use animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FItemAnim UseAnim;

	/** single fire sound (bLoopedFireSound not set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireSound;

	/** looped fire sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireLoopSound;

	/** finished burst sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireFinishSound;

	/** reload sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* ReloadSound;

	/** reload animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FItemAnim ReloadAnim;

	/** equip sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* EquipSound;

	/** fire animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FItemAnim FireAnim;

	/** is muzzle FX looped? */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		uint32 bLoopedMuzzleFX : 1;

	/** is fire sound looped? */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		uint32 bLoopedFireSound : 1;

	/** is fire animation looped? */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		uint32 bLoopedFireAnim : 1;

	/** is fire animation playing? */
	uint32 bPlayingFireAnim : 1;

public:

	FItemAnim GetItemEquipAnim();
	FItemConfig GetItemConfig();

public:


	//////////////////////////////////////////////////////////////////////////
	// Inventory

#pragma region Inventory

	/** attaches items mesh to pawn's mesh */
	void AttachMeshToPawn();

	/** detaches items mesh from pawn */
	void DetachMeshFromPawn();

	virtual void OnEquip();

	/** item is now equipped by owner pawn */
	virtual void OnEquipFinished();

	virtual void OnUnEquip();

	virtual void OnUnEquipFinished();

	void OnEnterInventory(class AMercuryCharacter* NewOwner);
	void OnLeaveInventory();

#pragma endregion

#pragma region Inventory Client Functions

	//////////////////////////////////////////////////////////////////////////
	// Inventory Functions called by the client and run on client
	void Client_OnEquip();
	/** weapon is now equipped by owner pawn */
	void Client_OnEquipFinished();

	void Client_OnUnEquip();

	void Client_OnEnterInventory(class AMercuryCharacter* NewOwner);
	void Client_OnLeaveInventory();

#pragma endregion

#pragma region Inventory Server Functions

	//////////////////////////////////////////////////////////////////////////
	// Inventory Functions called on the server
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnEquip();

	UFUNCTION(Server, Reliable, WithValidation)
	/** weapon is now equipped by owner pawn */
	void Server_OnEquipFinished();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnUnEquip();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnEnterInventory(class AMercuryCharacter* NewOwner);
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnLeaveInventory();

#pragma endregion

	//////////////////////////////////////////////////////////////////////////
	// Animations

#pragma region Animation Functions

	protected:
		/** play item animations */
		float PlayItemAnimation(const FItemAnim& Animation);

		/** stop playing item animations */
		void StopItemAnimation(const FItemAnim& Animation);

		/** play weapon sounds */
		UAudioComponent* PlayItemSound(USoundCue* Sound);

#pragma endregion


};
