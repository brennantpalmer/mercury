// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MuzzleFlashEffect.generated.h"

UCLASS()
class MERCURY_API AMuzzleFlashEffect : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMuzzleFlashEffect();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	/** default impact FX used when material specific override doesn't exist */
	UPROPERTY(EditDefaultsOnly, Category = Defaults)
		UParticleSystem* DefaultFX;

	/** spawn effect */
	virtual void PostInitializeComponents() override;
	
};
