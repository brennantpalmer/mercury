// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Runtime/Engine/Public/CollisionQueryParams.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "DrawDebugHelpers.h"
#include "BTPU.generated.h"

/**
*
*/
UCLASS()
class MERCURY_API UBTPU : public UObject
{
	GENERATED_BODY()

public:

	static FORCEINLINE bool TraceByChannel(
		UWorld* World,
		AActor* ActorToIgnore,
		const FVector& Start,
		const FVector& End,
		FHitResult& HitOut,
		ECollisionChannel CollisionChannel = ECC_Pawn,
		bool ReturnPhysMat = false
	) {
		if (!World)
		{
			return false;
		}

		FCollisionQueryParams TraceParams(FName(TEXT("BTPU Trace")), true, ActorToIgnore);
		TraceParams.bTraceComplex = true;
		//TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;
		const FName Debug("Debug");
		TraceParams.TraceTag = Debug;
		//Ignore Actors
		TraceParams.AddIgnoredActor(ActorToIgnore);


		FCollisionResponseParams ResponseParams;
		//Re-initialize hit info
		HitOut = FHitResult(ForceInit);

		//Trace!

		/*UWorld* const W = GetWorld();*/

		/*DrawDebugLine(
		World,
		Start,
		End,
		FColor(255, 0, 0),
		false, 15.f, 0,
		12.333
		);*/

		return World->LineTraceSingleByChannel(


			HitOut,		//result
			Start,	//start
			End, //end
			CollisionChannel, //collision channel
			TraceParams,
			ResponseParams
		);

		////Hit any Actor?
		//return (HitOut.GetActor() != NULL);
	}



};


class DebugHelper
{

public:

	FString ClassName;
	FString ObjectName;
	FString FunctionName;
	FString FunctionMessage;
	bool bShouldPrintServer;
	bool bShouldPrintClient;
	bool bShouldPrint;
	AActor* Owner;

	DebugHelper()
	{
		ClassName = "DefaultClassName";
		ObjectName = "DefaultObjectName";
		FunctionName = "DefaultFunctionName";
		FunctionMessage = "DefaultFunctionMessage";
		bShouldPrintServer = true;
		bShouldPrintClient = true;
		bShouldPrint = true;
		Owner = NULL;
	}

	// Owner must be set for this function to work
	void PrintToConsole(FString message = "")
	{
		if (bShouldPrint)
		{

			if (Owner)
			{
				FString printString;

				if (Owner->HasAuthority())
				{
					if (bShouldPrintServer)
					{
						printString = " <---[DEBUG_HELPER]---> AUTHORITY     |" + ClassName + "|" + ObjectName + "|" + FunctionName + "|" + message;
						UE_LOG(LogTemp, Warning, TEXT("%s"), *printString);
					}
				}
				else if (Owner->Role == ROLE_AutonomousProxy && bShouldPrintClient)
				{
					printString = " <---[DEBUG_HELPER]---> OWNING  CLIENT|" + ClassName + "|" + ObjectName + "|" + FunctionName + "|" + message;
					UE_LOG(LogTemp, Warning, TEXT("%s"), *printString);
				}
				else if (Owner->Role == ROLE_SimulatedProxy)
				{
					printString = " <---[DEBUG_HELPER]---> PROXY   CLIENT|" + ClassName + "|" + ObjectName + "|" + FunctionName + "|" + message;
					UE_LOG(LogTemp, Warning, TEXT("%s"), *printString);
				}
			}
			else
			{
				FString printString = " <---[DEBUG_HELPER]---> UNKNOWN CLIENT|" + ClassName + "|" + ObjectName + "|" + FunctionName + "|" + message;
				UE_LOG(LogTemp, Warning, TEXT("%s"), *printString);
			}
		}

	}
};


class FPlayerUsableState
{

	class APlayerUsable* MyOwner;

#pragma region Boolean States

private:

	    bool bIsOwned;
		bool bIsAttached;
		bool bIsEuipped;
		bool bPlayerTryingToUse;
		bool bIsInAutomaticMode;
		bool bIsInBurstMode;
		bool bIsInSingleMode;

#pragma endregion

public:
	FPlayerUsableState();

	// Interface for the PlayerUsable class
#pragma region Interface for PlayerUsable class

	// When called, switched the items use mode (Single, Burst, Auto) based on what the current mode is
	void Try_SwitchUseMode();

	// Set Owning player usable class
	void SetMyOwner(class APlayerUsable* NewOwner);
	void TrySetIsAttached(bool newValue);
	void TrySetIsOwned(bool newValue);
	void TrySetIsEquipped(bool newValue);
	void TrySetPlayerTryingToUse(bool newValue);
	void TrySetIsInAutoMode(bool newValue);
	void TrySetIsInBurstMode(bool newValue);
	void TrySetIsInSingleMode(bool newValue);

#pragma endregion

	void Client_TrySetIsOwned(bool newValue);
	void Client_TrySetIsAttached(bool newValue);
	void Client_TrySetIsEquipped(bool newValue);
	void Client_TrySetPlayerTryingToUse(bool newValue);
	void Client_TrySetIsInAutoMode(bool newValue);
	void Client_TrySetIsInBurstMode(bool newValue);
	void Client_TrySetIsInSingleMode(bool newValue);

	// Theses functions return the state booleans
#pragma region State Getters

	bool GetIsOwned();
	bool GetIsAttached();
	bool GetIsEquipped();
	bool GetPlayerTryingToUse();
	bool GetIsInAutoMode();
	bool GetIsInBurstMode();
	bool GetIsInSingleMode();

#pragma endregion

};